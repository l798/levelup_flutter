import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'components/splashscreen.dart';
import 'utils/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    //   statusBarBrightness: Brightness.dark,
    // ));
    final ThemeData theme = ThemeData();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'LevelUp',
      theme: theme.copyWith(
        appBarTheme:
            const AppBarTheme(systemOverlayStyle: SystemUiOverlayStyle.light),
        canvasColor: BK_APP,
        colorScheme: theme.colorScheme.copyWith(secondary: ACCENT_COLOR),
        textTheme: Theme.of(context)
            .textTheme
            .apply(bodyColor: TEXT_COLOR, fontFamily: 'OpenSans'),
        iconTheme: Theme.of(context).iconTheme.copyWith(color: ICON_COLOR),
      ),
      initialRoute: "/",
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => const Splashscreen(),
      },
    );
  }
}
