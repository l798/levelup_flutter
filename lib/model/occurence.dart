import './valid_enum.dart';

const String tableOccurences = 'occurences';

class OccurenceField {
  static const String idHabit = '_idHabit';
  static const String date = 'date';
  static const String valid = 'valid';
}

class Occurence {
  late int idHabit;
  late DateTime date;
  late ValidEnum valid;

  Occurence(this.idHabit, {DateTime? date, ValidEnum? valid}) {
    this.date = date ?? DateTime.now();
    this.valid = valid ?? ValidEnum.WAITING;
  }

  int getIdOccurence() {
    return idHabit;
  }

  DateTime getDate() {
    return date;
  }

  void setDate(DateTime date) {
    this.date = date;
  }

  ValidEnum getValidState() {
    if (valid != ValidEnum.YES) {
      DateTime dateNow = DateTime.now();
      if (dateNow.isAfter(date)) {
        valid = ValidEnum.NO;
      } else {
        valid = ValidEnum.WAITING;
      }
    }
    return valid;
  }

  void setValidState(ValidEnum valid) {
    this.valid = valid;
  }

  Occurence copy(int idHabit, DateTime date, {ValidEnum? valid}) {
    return Occurence(idHabit, date: date, valid: valid ?? ValidEnum.WAITING);
  }

  // convert object to json
  Map<String, Object?> toJson() => {
        OccurenceField.idHabit: idHabit,
        OccurenceField.date: date.toIso8601String(),
        OccurenceField.valid.toString(): valid.index
      };

  static Occurence fromJson(Map<String, Object?> json) {
    return Occurence(json[OccurenceField.idHabit] as int,
        date: DateTime.parse(json[OccurenceField.date] as String),
        valid: ValidEnum.values[json[OccurenceField.valid] as int]);
  }
}
