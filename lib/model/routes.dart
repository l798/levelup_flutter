import 'package:flutter/material.dart';
import 'package:levelup/widget/week/week_widget.dart';
import 'package:levelup/widget/habits_widget.dart';
import 'package:levelup/widget/agenda/agenda_widget.dart';

import '../utils/theme.dart';
import '../widget/stats_widget.dart';
import 'route_model.dart';

class Routes {
  Routes._();
  // liste des routes de l'application
  static List<RouteModel> routes = [
    RouteModel(
        name: "Ma semaine",
        icon: const Icon(Icons.today),
        widget: WeekComponent(),
        backgroundColor: BOTTOMBAR_BK),
    RouteModel(
        name: "Agenda",
        icon: const Icon(Icons.home),
        widget: Agenda(),
        backgroundColor: BOTTOMBAR_BK),
    RouteModel(
        name: "Liste d'Habitude",
        icon: const Icon(Icons.repeat),
        widget: const Habits(),
        backgroundColor: BOTTOMBAR_BK),
    RouteModel(
        name: "Statistiques",
        icon: const Icon(Icons.bar_chart),
        widget: StatisticalComponent(),
        backgroundColor: BOTTOMBAR_BK),
  ];
}


/* pour faire un singleton en dart
class Singleton {
  static final Singleton _singleton = Singleton._internal();

  factory Singleton() {
    return _singleton;
  }

  Singleton._internal();
}
*/
