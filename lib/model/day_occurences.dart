import 'package:levelup/model/habit.dart';

import 'occurence.dart';

class DayOccurences {
  late Habit habit;
  late Occurence occurence;

  DayOccurences(this.habit, this.occurence);
}
