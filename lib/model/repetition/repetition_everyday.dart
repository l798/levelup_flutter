import 'repetition.dart';

class RepetitionEveryDay extends Repetition{

  RepetitionEveryDay() : super(RepetitionType.EVERYDAY);

  @override
  RepetitionType getType() {
    return this.type;
  }

  @override
  String getRepetitionTypeFromString() {
    return "Everyday";
  }

  @override
  String toString(){
    return "Repetition:{type:Everyday}";
  }

  @override
  DateTime getLimitTimeForIsValid(DateTime dateEnd){
    DateTime now = DateTime.now();
    return DateTime(now.year, now.month, now.day, dateEnd.hour, dateEnd.minute);
  }

  @override
  DateTime getFormerTimeForIsValid(DateTime dateStart){
    return DateTime.now().subtract(Duration(days: 1));
  }

  @override
  String getRepetitionValueDB(){
    return "daily";
  }
}