enum RepetitionType {
  NO_REPEAT, EVERYDAY, ONCE_A_WEEK
}

class RepetitionTypes {
  static const String NO_REPEAT_FR = "Pas de répétition";
  static const String ONCE_A_WEEK_FR = "Répéter une fois par semaine";
  static const String EVERYDAY_FR = "Répéter une fois par jour";

  static const String NO_REPEAT_EN = "No repeat";
  static const String ONCE_A_WEEK_EN = "Once a week";
  static const String EVERYDAY_EN = "Everyday";

  static List<String> listFR = [NO_REPEAT_FR, ONCE_A_WEEK_FR, EVERYDAY_FR];
}

abstract class Repetition{
  RepetitionType type = RepetitionType.NO_REPEAT;

  Repetition(RepetitionType type){
    this.type = type;
  }

  RepetitionType getType();
  String getRepetitionTypeFromString();
  DateTime getLimitTimeForIsValid(DateTime dateEnd);
  DateTime getFormerTimeForIsValid(DateTime dateStart);
  String getRepetitionValueDB();
}