import '../repetition/repetition.dart';

import 'repetition.dart';

class RepetitionNoRepeat extends Repetition{
  RepetitionNoRepeat() : super(RepetitionType.NO_REPEAT);

  @override
  String getRepetitionTypeFromString() {
    return "No repeat";
  }

  @override
  String toString(){
    return "Repetition:{type:No repeat}";
  }

  @override
  DateTime getLimitTimeForIsValid(DateTime dateEnd){
    return dateEnd;
  }

  @override
  DateTime getFormerTimeForIsValid(DateTime dateStart){
    return dateStart;
  }

  @override
  String getRepetitionValueDB(){
    return "";
  }

  @override
  RepetitionType getType() {
    return type;
  }


}