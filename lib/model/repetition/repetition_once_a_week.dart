import '../repetition/repetition.dart';

import 'repetition.dart';

enum DaysWeek{
  MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

class RepetitionOnceAWeek extends Repetition{

  DaysWeek daysWeek = DaysWeek.MONDAY;

  RepetitionOnceAWeek(DaysWeek daysWeek) : super(RepetitionType.ONCE_A_WEEK){
    this.daysWeek = daysWeek;
  }

  // English
  static const String MONDAY_EN = "monday";
  static const String TUESDAY_EN = "tuesday";
  static const String WEDNESDAY_EN = "wednesday";
  static const String THURSDAY_EN = "thursday";
  static const String FRIDAY_EN = "friday";
  static const String SATURDAY_EN = "saturday";
  static const String SUNDAY_EN = "sunday";

  // French
  static const String MONDAY_FR = "Lundi";
  static const String TUESDAY_FR = "Mardi";
  static const String WEDNESDAY_FR = "Mercredi";
  static const String THURSDAY_FR = "Jeudi";
  static const String FRIDAY_FR = "Vendredi";
  static const String SATURDAY_FR = "Samedi";
  static const String SUNDAY_FR = "Dimanche";

  static List<String> listEN = [
    MONDAY_EN,
    TUESDAY_EN,
    WEDNESDAY_EN,
    THURSDAY_EN,
    FRIDAY_EN,
    SATURDAY_EN,
    SUNDAY_EN
  ];

  static List<String> listFR = [
    MONDAY_FR,
    TUESDAY_FR,
    WEDNESDAY_FR,
    THURSDAY_FR,
    FRIDAY_FR,
    SATURDAY_FR,
    SUNDAY_FR
  ];

  RepetitionOnceAWeek.inputString(String daysWeek) : super(RepetitionType.ONCE_A_WEEK){
    DaysWeek d;
    switch(daysWeek){
      case MONDAY_EN: {d = DaysWeek.MONDAY;} break;
      case TUESDAY_EN: {d = DaysWeek.TUESDAY;} break;
      case WEDNESDAY_EN: {d = DaysWeek.WEDNESDAY;} break;
      case THURSDAY_EN: {d = DaysWeek.THURSDAY;} break;
      case FRIDAY_EN: {d = DaysWeek.FRIDAY;} break;
      case SATURDAY_EN: {d = DaysWeek.SATURDAY;} break;
      case SUNDAY_EN: {d = DaysWeek.SATURDAY;} break;
      default: {d = DaysWeek.SUNDAY;} break;
    }
    this.daysWeek = d;
  }

  @override
  RepetitionType getType() {
    return type;
  }

  String getRepetitionTypeFromString() {
    return "Once a week";
  }

  String toString(){
    return "Repetition:{type:Once a week, day:" + daysWeek.toString() + "}";
  }

  DateTime getLimitTimeForIsValid(DateTime dateEnd){
    DateTime now = DateTime.now();
    if(now.isBefore(dateEnd)){
      return now.add(Duration(days: (this.daysWeek.index+1-now.weekday)));
    }
    return DateTime(dateEnd.year, dateEnd.month, dateEnd.day, dateEnd.hour, dateEnd.minute);
  }

  @override
  DateTime getFormerTimeForIsValid(DateTime dateStart){
    DateTime now = DateTime.now();
    if(now.isAfter(dateStart)){
      int value = this.daysWeek.index+1-now.weekday;
      if(value > 0) value = 7-value;
      value = (value + 1).abs();
      return now.subtract(Duration(days: value));
    }
    return DateTime(dateStart.year, dateStart.month, dateStart.day, dateStart.hour, dateStart.minute);
  }

  String getRepetitionValueDB(){
    switch(daysWeek){
      case DaysWeek.MONDAY : {return MONDAY_EN;} break;
      case DaysWeek.TUESDAY : {return TUESDAY_EN;} break;
      case DaysWeek.WEDNESDAY : {return WEDNESDAY_EN;} break;
      case DaysWeek.THURSDAY : {return THURSDAY_EN;} break;
      case DaysWeek.FRIDAY : {return FRIDAY_EN;} break;
      case DaysWeek.SATURDAY : {return SATURDAY_EN;} break;
      default : {return SUNDAY_EN;} break;
    }
  }
}