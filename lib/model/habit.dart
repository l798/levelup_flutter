import 'package:levelup/database/occurence_database.dart';

import '../../model/repetition/repetition.dart';
import '../../model/repetition/repetition_everyday.dart';
import '../../model/repetition/repetition_no_repeat.dart';
import '../../model/repetition/repetition_once_a_week.dart';
import 'occurence.dart';
import '../model/valid_enum.dart';

const String tableHabits = 'habits';

class HabitFields {
  static final List<String> values = [
    id,
    title,
    description,
    repetition,
    isAlert,
    startDate,
    endDate
  ];

  static const String id = '_id';
  static const String title = 'title';
  static const String description = 'description';
  static const String repetition = 'repetition';
  static const String isAlert = 'isAlert';
  static const String startDate = 'startDate';
  static const String endDate = 'endDate';
}

class Habit {
  final OccurenceDatabase occurenceDB = OccurenceDatabase();
  late int?
      id; // If you have an ID, you pass it inside, it will generate this ID in the database
  late String title; // title of the habit
  late String description; // description of the habit
  late bool isAlert; // Yes: alert one hour before; No: no alert
  late DateTime startDate; // Start date of the habit
  late DateTime endDate; // End date of the habit
  Repetition repetition = RepetitionNoRepeat();

  Habit(
      {this.id,
      String? title,
      String? description,
      DateTime? dateStart,
      DateTime? dateEnd,
      bool? alert,
      Repetition? repetition}) {
    //this.id = id;
    this.title = title ?? "";
    this.description = description ?? "";
    startDate = dateStart ?? DateTime.now();
    endDate = dateEnd ?? DateTime.now();
    isAlert = alert ?? false;
    this.repetition = repetition ?? RepetitionNoRepeat();
  }

  // STEP1: repetition
  Habit.fromDB(
      {this.id,
      String? title,
      String? description,
      DateTime? dateStart,
      DateTime? dateEnd,
      bool? alert,
      String? repetition}) {
    //this.id = id ?? null;
    this.title = title ?? "";
    this.description = description ?? "";
    startDate = dateStart ?? DateTime.now();
    endDate = dateEnd ?? DateTime.now();
    isAlert = alert ?? false;
    this.repetition = stringToRepetition(repetition ?? "");
  }

  int getId() {
    return id ?? -1;
  }

  String getTitle() {
    return title;
  }

  void setTitle(String title) {
    this.title = title;
  }

  String getDescription() {
    return description;
  }

  void setDescription(String description) {
    this.description = description;
  }

  DateTime getDateStart() {
    return startDate;
  }

  void setDateStart(DateTime dateStart) {
    startDate = dateStart;
  }

  DateTime getDateEnd() {
    return endDate;
  }

  void setDateEnd(DateTime dateEnd) {
    endDate = dateEnd;
  }

  bool getAlert() {
    return isAlert;
  }

  void setAlert(bool alert) {
    isAlert = alert;
  }

  Repetition getRepetition() {
    return repetition;
  }

  String getRepetitionFromString() {
    return repetition.getRepetitionTypeFromString();
  }

  void setRepetition(Repetition repetition) {
    this.repetition = repetition;
  }

  bool initialisationListHabit() {
    if (occurenceDB == null) {
      if (id == null) {
        // TODO - return error
        print(
            "We cannot make the request, the habit ID is not assigned, please save in the database and assign the ID before you can make this request");
        return false;
      }
    }
    return true;
  }

  ValidEnum boolToIsValid(bool value) {
    if (value) {
      return ValidEnum.YES;
    } else if (DateTime.now().isBefore(getLimitTimeForIsValid())) {
      return ValidEnum.WAITING;
    } else {
      return ValidEnum.NO;
    }
  }

  ValidEnum getIsValid() {
    if (initialisationListHabit()) {
      List<Occurence> l = [];
      occurenceDB
          .readOccurencesBetweenDateByIdHabit(
              id!, getFormerTimeForIsValid(), getLimitTimeForIsValid())
          .then((value) {
        l = value;
        if (l.isEmpty) {
          //setIsValid(IsValid.NO, getLimitTimeForIsValid());
          return ValidEnum.NO;
        }
        //return boolToIsValid(l[l.length - 1].valid);
        return true;
      });
    }
    return ValidEnum.WAITING;
  }

  String getIsValidFromString() {
    ValidEnum value = getIsValid();
    switch (value) {
      case ValidEnum.NO:
        {
          return "No";
        }
      case ValidEnum.WAITING:
        {
          return "Waiting";
        }
      case ValidEnum.YES:
        {
          return "Yes";
        }
      default:
        {
          return "No";
        }
    }
  }

  void setIsValid(ValidEnum isValid, DateTime date) {
    final lFuture = occurenceDB.readOccurencesBetweenDateByIdHabit(
        id!, getFormerTimeForIsValid(), getLimitTimeForIsValid());
    List<Occurence> l = [];
    lFuture.then((value) {
      l = value;
    });
    DateTime d = DateTime.now();
    ValidEnum isValid = ValidEnum.WAITING;
    if (isValid == ValidEnum.YES) {
      d = getLimitTimeForIsValid();
      isValid = ValidEnum.YES;
    }
    if (l.isEmpty) {
     // occurenceDB.create(Occurence(id!, date: d, isValid: isValid));
    } else {
      Occurence value = l[l.length - 1];
      value.valid = isValid;
      occurenceDB.update(value);
    }
  }

  DateTime getLimitTimeForIsValid() {
    return repetition.getLimitTimeForIsValid(endDate);
  }

  DateTime getFormerTimeForIsValid() {
    return repetition.getFormerTimeForIsValid(startDate);
  }

  Habit copy(
      {int? id,
      String? title,
      String? description,
      String? repetition,
      bool? isAlert,
      DateTime? startDate,
      DateTime? endDate}) {
    if (repetition != null) {
      return Habit.fromDB(
          id: id,
          title: title ?? this.title,
          description: description ?? this.description,
          dateStart: startDate ?? this.startDate,
          dateEnd: endDate ?? this.endDate,
          alert: isAlert ?? this.isAlert,
          repetition: repetition);
    } else {
      return Habit(
          id: id,
          title: title ?? this.title,
          description: description ?? this.description,
          dateStart: startDate ?? this.startDate,
          dateEnd: endDate ?? this.endDate,
          alert: isAlert ?? this.isAlert,
          repetition: this.repetition);
    }
  }

  // convert object to json
  Map<String, Object?> toJson() => {
        HabitFields.id: id,
        HabitFields.title: title,
        HabitFields.description: description,
        HabitFields.repetition: repetition.getRepetitionValueDB(),
        HabitFields.isAlert: isAlert ? 1 : 0,
        HabitFields.startDate: startDate.toIso8601String(),
        HabitFields.endDate: endDate.toIso8601String(),
      };

  static Habit fromJson(Map<String, Object?> json) {
    return Habit.fromDB(
        id: json[HabitFields.id] as int?,
        title: json[HabitFields.title] as String,
        description: json[HabitFields.description] as String,
        dateStart: DateTime.parse(json[HabitFields.startDate] as String),
        dateEnd: DateTime.parse(json[HabitFields.endDate] as String),
        alert: json[HabitFields.isAlert] == 1,
        repetition: json[HabitFields.repetition] as String);
  }

  Repetition stringToRepetition(String repetition) {
    if (repetition == "") {
      return RepetitionNoRepeat();
    } else if (repetition == "daily") {
      return RepetitionEveryDay();
    } else {
      return RepetitionOnceAWeek.inputString(repetition);
    }
  }
}
