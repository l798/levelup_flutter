import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// classe de définition d'une route
class RouteModel {
  String name;
  Icon icon;
  Widget widget;
  Color backgroundColor;

  RouteModel({
    required this.name,
    required this.icon,
    required this.widget,
    required this.backgroundColor,
  });
}
