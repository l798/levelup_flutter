// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';

/// Custom Widget pour ajouter l'appel d'une méthode par le state
abstract class CustomStateFullWidget extends StatefulWidget {
  CustomStateFullWidget({Key? key}) : super(key: key);
  late Function stateMethod;
  void refresh();
}
