import '../model/occurence.dart';
import 'database.dart';
import '../helpers/date_time_helper.dart';

class OccurenceDatabase {
  static final HabitsDB instance = HabitsDB.instance;

  // ----------------
  // CRUD OPERATIONS
  // ----------------

  Future<Occurence> create(Occurence occurence) async {
    await instance.insert(tableOccurences, occurence.toJson());
    return occurence;
  }

  Future<List<Occurence>> readOccurencesBetweenDate(DateTime dateStart, DateTime dateEnd) async {
    const orderBy = '${OccurenceField.date} DESC';
    //const where = '${OccurenceField.date} BETWEEN ? and ?';
    const where = '${OccurenceField.date} >= ? and ${OccurenceField.date} < ?';
    final result = await instance.query(tableOccurences,
        where: where,
        whereArgs: [
          DateTimeHelper.dateTimeForDatabase(dateStart),
          DateTimeHelper.dateTimeForDatabase(dateEnd)
        ],
        orderBy: orderBy);
    return result.map((json) => Occurence.fromJson(json)).toList();
  }

  Future<List<Occurence>> readOccurencesBetweenDateByIdHabit(
      int idHabit, DateTime dateStart, DateTime? dateEnd) async {
    dateEnd = dateEnd ??
        DateTime(dateStart.year, dateStart.month, dateStart.day, 23, 59, 59);

    const orderBy = '${OccurenceField.date} DESC';
    const where =
        '${OccurenceField.idHabit} = ? and (${OccurenceField.date} BETWEEN ? and ?)';
    final result = await instance.query(tableOccurences,
        where: where,
        whereArgs: [
          idHabit,
          DateTimeHelper.dateTimeForDatabase(dateStart),
          DateTimeHelper.dateTimeForDatabase(dateEnd)
        ],
        orderBy: orderBy);
    return result.map((json) => Occurence.fromJson(json)).toList();
  }

  Future<List<Occurence>> readAll() async {
    const orderBy = '${OccurenceField.date} DESC';
    final result = await instance.query(tableOccurences, orderBy: orderBy);
    return result.map((json) => Occurence.fromJson(json)).toList();
  }

  Future<List<Occurence>> readAllByIdHabit(int idHabit) async {
    const orderBy = '${OccurenceField.date} DESC';
    const where = '${OccurenceField.idHabit} = ?';
    final result = await instance.query(tableOccurences,
        orderBy: orderBy, where: where, whereArgs: [idHabit]);
    return result.map((json) => Occurence.fromJson(json)).toList();
  }

  Future<List<Occurence>> readAllOccurences() async {
    const orderBy = '${OccurenceField.date} DESC';

    // MAKE YOUR OWN QUERY
    // -------------------
    // final result = db.rawQuery('SELECT * FROM $tableHabits ORDER BY $orderBy');

    // HERE IS THE ERROR
    final result = await instance.query(tableOccurences, orderBy: orderBy);
    return result.map((json) => Occurence.fromJson(json)).toList();
  }

  Future<int> update(Occurence occurence) async {
    // You can use db.raw for an sql statement
    return instance.update(
      tableOccurences,
      occurence.toJson(),
      where: '${OccurenceField.idHabit} = ? and ${OccurenceField.date} = ?',
      whereArgs: [
        occurence.idHabit,
        DateTimeHelper.dateTimeForDatabase(occurence.date)
      ],
    );
  }

  Future<int> delete(int id) async {
    // final db = await instance.database;

    // return instance.delete(
    //   tableListHabits,
    //   where: '${ListHabitFields.id_habit} = ? and ${ListHabitFields.date}',
    //   whereArgs: [id, date],
    // );
    return instance.delete(
      tableOccurences,
      where: '${OccurenceField.idHabit} = ?',
      whereArgs: [id],
    );
  }
}
