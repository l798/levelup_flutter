import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import '../model/habit.dart';
import 'package:levelup/model/occurence.dart';

class HabitsDB {
  static final HabitsDB instance = HabitsDB._init();
  static Database? _database;
  HabitsDB._init();

  // instantiate the database
  Future<Database> get database async {
    if (_database != null) return _database!;

    // if it is existing, we do not create it
    _database = await _initDB('habits.db');
    return _database!;
  }

  // open the database
  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path,
        version: 1, onCreate: _createDB, onConfigure: _onConfigure);
  }

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  // creates the database
  Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const textType = 'TEXT NOT NULL';
    const boolType = 'BOOLEAN NOT NULL';
    const integerType = 'INTEGER NOT NULL';

    await db.execute('''

CREATE TABLE $tableHabits (
  ${HabitFields.id} $idType,
  ${HabitFields.title} $textType,
  ${HabitFields.description} $textType,
  ${HabitFields.repetition} $textType,
  ${HabitFields.isAlert} $boolType,
  ${HabitFields.startDate} $textType,
  ${HabitFields.endDate} $textType
  );''');

    await db.execute('''
    CREATE TABLE $tableOccurences (
    ${OccurenceField.idHabit} $integerType,
    ${OccurenceField.date} $textType,
    ${OccurenceField.valid} $integerType,
    FOREIGN KEY (${OccurenceField.idHabit}) REFERENCES $tableHabits (${HabitFields.id}) ON DELETE NO ACTION ON UPDATE NO ACTION,
    PRIMARY KEY ( ${OccurenceField.idHabit}, ${OccurenceField.date})
    );''');
  }

  Future<int> insert(String table, Map<String, Object?> values) async {
    final db = await instance.database;
    final id = db.insert(table, values, conflictAlgorithm: ConflictAlgorithm.replace);
    return id;
  }

  Future<List<Map<String, Object?>>> rawQuery(String request) async {
    final db = await instance.database;
    final result = await db.rawQuery(request);
    return result;
  }

  Future<List<Map<String, Object?>>> query(String table,
      {bool? distinct,
      List<String>? columns,
      String? where,
      List<Object?>? whereArgs,
      String? groupBy,
      String? having,
      String? orderBy,
      int? limit,
      int? offset}) async {
    final db = await instance.database;
    return db.query(table,
        distinct: distinct,
        columns: columns,
        where: where,
        whereArgs: whereArgs,
        groupBy: groupBy,
        having: having,
        orderBy: orderBy,
        limit: limit,
        offset: offset);
  }

  Future<int> update(String table, Map<String, Object?> values,
      {String? where,
      List<Object?>? whereArgs,
      ConflictAlgorithm? conflictAlgorithm}) async {
    final db = await instance.database;
    return db.update(table, values,
        where: where,
        whereArgs: whereArgs,
        conflictAlgorithm: conflictAlgorithm);
  }

  Future<int> delete(String table,
      {String? where, List<Object?>? whereArgs}) async {
    final db = await instance.database;
    return db.delete(table, where: where, whereArgs: whereArgs);
  }

  // close the database
  Future close() async {
    final db = await instance.database;

    db.close();
  }
}
