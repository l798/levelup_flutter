import 'package:levelup/model/occurence.dart';
import 'package:levelup/model/valid_enum.dart';
import '../model/occurence.dart';
import '../../model/repetition/repetition.dart';
import 'package:levelup/widget/habits_widget.dart';

import '../model/habit.dart';
import 'database.dart';

class HabitsDatabase {
  static final HabitsDB instance = HabitsDB.instance;
  // ----------------
  // CRUD OPERATIONS
  // ----------------
  Future<Habit> create(Habit habit) async {
    // On ajoute l'habitude à la base et on récupère l'id pour créer les occurences
    final id = await instance.insert(tableHabits, habit.toJson());

    bool endLoop = false;
    DateTime repetitionDate = habit.startDate;
    int dayInc = 0;
    // En fonction du type de répétition on défini le nombre de jour entre chaque occurence
    switch (habit.repetition.getType()) {
      case RepetitionType.NO_REPEAT:
        dayInc = 0;
        break;
      case RepetitionType.EVERYDAY:
        dayInc = 1;
        break;
      case RepetitionType.ONCE_A_WEEK:
        dayInc = 7;
        break;
    }

    do {
      // Création d'une Liste d'occurences (occurence)
      Occurence occurences =
          Occurence(id, date: repetitionDate, valid: ValidEnum.WAITING);
      var resultOccurence = await habit.occurenceDB.create(occurences);

      print(resultOccurence.toJson());
      // on incrémente la date en cours par le nombre de jour entre chaque occurence
      repetitionDate = repetitionDate.add(Duration(days: dayInc));

      // Si la date dépasse la date de fin de l'habitude ou que dayInc == 00 (pas de répétition)
      // alors on quitte la boucle
      if (repetitionDate.isAfter(habit.endDate) || dayInc == 0) {
        endLoop = true;
      }
    } while (!endLoop);
    return habit.copy(id: id);
  }

  // readHabit(DateTime time)
  Future<Habit> readHabit(int id) async {
    // final db = await instance.database;

    // columns that you want to retrieve from your table
    final maps = await instance.query(tableHabits,
        columns: HabitFields.values,
        where: '${HabitFields.id} = ?',
        whereArgs: [id]);

    // convert the map into a habit object
    if (maps.isNotEmpty) {
      return Habit.fromJson(maps.first);
    } else {
      throw Exception("ID $id is not found");
    }
  }

  Future<List<Habit>> readAllHabits() async {
    //final db = await instance.database;

    const orderBy = '${HabitFields.startDate} DESC';

    // MAKE YOUR OWN QUERY
    // -------------------
    // final result = db.rawQuery('SELECT * FROM $tableHabits ORDER BY $orderBy');

    // HERE IS THE ERROR
    final result = await instance.query(tableHabits, orderBy: orderBy);
    return result.map((json) => Habit.fromJson(json)).toList();
  }

  Future<List<Habit>> getHabitsByDateTime(DateTime date) async {
    const orderBy = '${HabitFields.startDate} ASC';
    final dateString = date.toIso8601String();
    final result = await instance.query(tableHabits,
        orderBy: orderBy,
        where: '${HabitFields.startDate} <= ? AND ${HabitFields.endDate} >= ?',
        whereArgs: [dateString, dateString]);
    return result.map((json) => Habit.fromJson(json)).toList();
  }

  Future<List<Habit>> getHabitsByDay(DateTime day) async {
    const orderBy = '${HabitFields.startDate} ASC';
    final dateString = DateTime(day.year, day.month, day.day).toIso8601String();
    final endDayString =
        DateTime(day.year, day.month, day.day, 23, 59, 59).toIso8601String();
    final result = await instance.query(tableHabits,
        orderBy: orderBy,
        where: '${HabitFields.startDate} <= ? AND ${HabitFields.endDate} >= ?',
        whereArgs: [endDayString, dateString]);
    return result.map((json) => Habit.fromJson(json)).toList();
  }

  Future<int> update(Habit habit) async {
    // final db = await instance.database;

    // You can use db.raw for an sql statement
    return instance.update(
      tableHabits,
      habit.toJson(),
      where: '${HabitFields.id} = ?',
      whereArgs: [habit.id],
    );
  }

  Future<int> delete(int id) async {
    final result = await instance.delete(
      tableHabits,
      where: '${HabitFields.id} = ?',
      whereArgs: [id],
    );
    return result;
  }
}
