import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

abstract class DateTimeHelper {
  static String frenchDateFormat = 'dd/MM/yyyy';
  static String frenchTimeFormat = 'HH:mm';

  static DateTime convertToDateTime(DateTime date, TimeOfDay timeOfDay) =>
      DateTime(
          date.year, date.month, date.day, timeOfDay.hour, timeOfDay.minute);

  // Date conversions
  // ----------------
  static DateTime convertTextToDate(String date) {
    // dd(0)/MM(1)/yyyy(2)
    const int dayIndex = 0;
    const int monthIndex = 1;
    const int yearIndex = 2;

    List<String> splitedDate = date.split("/");
    int day = int.parse(splitedDate[dayIndex]);
    int month = int.parse(splitedDate[monthIndex]);
    int year = int.parse(splitedDate[yearIndex]);

    var resultDate = DateTime(year, month, day);
    return resultDate;
  }

  static String convertDateToTextForStorage(DateTime date) =>
      DateFormat.yMd().format(date);

  static String convertDateToTextForDisplay(DateTime date) =>
      DateFormat(frenchDateFormat).format(date);
  // ----------------

  // Time conversions
  // ----------------
  static TimeOfDay convertTextToTime(String time) {
    // /HH(1)/mm(2)
    const int hourIndex = 0;
    const int minuteIndex = 1;

    List<String> splitedTime = time.split("/");
    int hour = int.parse(splitedTime[hourIndex]);
    int minute = int.parse(splitedTime[minuteIndex]);

    return TimeOfDay(hour: hour, minute: minute);
  }

  static String convertTimeToTextForStorage(TimeOfDay time) {
    String currentHour = time.hour.toString();
    String currentMinute = time.minute.toString();
    return currentHour + ' : ' + currentMinute;
  }

  static String convertTimeToTextForDisplay(TimeOfDay time) {
    final now = DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, time.hour, time.minute);
    final format = DateFormat.jm(); //"6:00 AM"
    return format.format(dt);
  }

  ///** Convert dateTime year-month-day hour:minut:second:000
  /// to year-month-dayThour:minut:second:000
  /// ie : 2022-02-03 00:00:00.000 -> 2022-02-03T00:00:00.000*/
  static String dateTimeForDatabase(DateTime date) {
    return date.toString().replaceFirst(" ", "T");
  }
// ----------------
}
