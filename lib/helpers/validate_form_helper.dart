abstract class ValidateForm {
  static CheckValidity checkTitle(String data) {
    bool isNotValid = false;
    String errorMessage = "";

    if (data.isEmpty) {
      errorMessage = "Veuillez saisir un titre";
      isNotValid = true;
    }
    if (data.length < 3) {
      errorMessage = "Le titre doit faire plus de 3 caractères";
      isNotValid = true;
    }
    if (data.length > 30) {
      errorMessage = "Le titre doit faire moins de 30 caractères";
      isNotValid = true;
    }

    return CheckValidity(isNotValid, errorMessage);
  }

  static CheckValidity checkDescription(String data) {
    bool isNotValid = false;
    String errorMessage = "";

    if (data.isEmpty) {
      errorMessage = "Veuillez saisir une description";
      isNotValid = true;
    }
    if (data.length < 5) {
      errorMessage = "La description doit faire plus de 5 caractères";
      isNotValid = true;
    }
    if (data.length > 255) {
      errorMessage = "La description doit faire plus de 255 caractères";
      isNotValid = true;
    }

    return CheckValidity(isNotValid, errorMessage);
  }

  static CheckValidity checkDates(DateTime startDate, DateTime endDate) {
    bool isNotValid = false;
    String errorMessage = "";

    if (startDate.isAfter(endDate)) {
      errorMessage = "La date de départ doit être avant la date de fin";
      isNotValid = true;
    }
    if (startDate.isAtSameMomentAs(endDate)) {
      errorMessage =
          "La date de départ et la date de fin doivent être différentes";
      isNotValid = true;
    }

    DateTime oneWeekAfterStart = startDate.add(const Duration(days: 1));
    if (endDate.isBefore(oneWeekAfterStart)) {
      errorMessage =
          "La durée d'une habitude doit être supèrieure à une journée";
      isNotValid = true;
    }

    return CheckValidity(isNotValid, errorMessage);
  }
}

class CheckValidity {
  bool isNotValid = false;
  String message = "";

  CheckValidity(this.isNotValid, this.message);
}
