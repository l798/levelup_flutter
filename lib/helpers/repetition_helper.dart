// ignore_for_file: non_constant_identifier_names

import '../../model/repetition/repetition.dart';
import '../../model/repetition/repetition_once_a_week.dart';

abstract class RepetitionHelper {
  static String ConvertRepetitionValueForDatabase(
      String repetitionModesValue, String repetitionDays) {
    String result = "";

    if (repetitionModesValue == RepetitionTypes.NO_REPEAT_FR) result = "";
    if (repetitionModesValue == RepetitionTypes.EVERYDAY_FR) result = "daily";
    if (repetitionModesValue == RepetitionTypes.ONCE_A_WEEK_FR) {
      switch (repetitionDays) {
        case RepetitionOnceAWeek.MONDAY_FR:
          result = RepetitionOnceAWeek.MONDAY_EN;
          break;
        case RepetitionOnceAWeek.TUESDAY_FR:
          result = RepetitionOnceAWeek.TUESDAY_EN;
          break;
        case RepetitionOnceAWeek.WEDNESDAY_FR:
          result = RepetitionOnceAWeek.WEDNESDAY_EN;
          break;
        case RepetitionOnceAWeek.THURSDAY_FR:
          result = RepetitionOnceAWeek.THURSDAY_EN;
          break;
        case RepetitionOnceAWeek.FRIDAY_FR:
          result = RepetitionOnceAWeek.FRIDAY_EN;
          break;
        case RepetitionOnceAWeek.SATURDAY_FR:
          result = RepetitionOnceAWeek.SATURDAY_EN;
          break;
        case RepetitionOnceAWeek.SUNDAY_FR:
          result = RepetitionOnceAWeek.SUNDAY_EN;
          break;
        default:
          result = "";
          break;
      }
    }

    return result;
  }

  static String ConvertRepetitionTypesEnToFr(String? repetitionTypeEn) {
    String result = "";
    switch (repetitionTypeEn) {
      case RepetitionTypes.NO_REPEAT_EN:
        result = RepetitionTypes.NO_REPEAT_FR;
        break;
      case RepetitionTypes.EVERYDAY_EN:
        result = RepetitionTypes.EVERYDAY_FR;
        break;
      case RepetitionTypes.ONCE_A_WEEK_EN:
        result = RepetitionTypes.ONCE_A_WEEK_FR;
        break;
      default:
        result = RepetitionTypes.NO_REPEAT_FR;
        break;
    }

    return result;
  }

  static String ConvertRepetitionDaysEnToFr(String? repetitionDayEn) {
    String result = "";

    switch (repetitionDayEn) {
      case RepetitionOnceAWeek.MONDAY_EN:
        result = RepetitionOnceAWeek.MONDAY_FR;
        break;
      case RepetitionOnceAWeek.TUESDAY_EN:
        result = RepetitionOnceAWeek.TUESDAY_FR;
        break;
      case RepetitionOnceAWeek.WEDNESDAY_EN:
        result = RepetitionOnceAWeek.WEDNESDAY_FR;
        break;
      case RepetitionOnceAWeek.THURSDAY_EN:
        result = RepetitionOnceAWeek.THURSDAY_FR;
        break;
      case RepetitionOnceAWeek.FRIDAY_EN:
        result = RepetitionOnceAWeek.FRIDAY_FR;
        break;
      case RepetitionOnceAWeek.SATURDAY_EN:
        result = RepetitionOnceAWeek.SATURDAY_FR;
        break;
      case RepetitionOnceAWeek.SUNDAY_EN:
        result = RepetitionOnceAWeek.SUNDAY_FR;
        break;
      default:
        result = "";
        break;
    }

    return result;
  }
}
