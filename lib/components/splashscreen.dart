import 'package:flutter/material.dart';
import '../components/homepage.dart';
import '../utils/theme.dart';

class Splashscreen extends StatefulWidget {
  const Splashscreen({Key? key}) : super(key: key);

  @override
  SplashscreenState createState() => SplashscreenState();
}

class SplashscreenState extends State<Splashscreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      // Création d'une future pour se diriger vers HomePage après un temps donné
      Future.delayed(const Duration(milliseconds: 2000), () {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return const Homepage();
        }));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        // A remplacer par un Gif avant prod
        child: Image.asset(
          "assets/images/levelup.gif",
          height: 480.0,
          width: 480.0,
        ),
        //   Text('LevelUp',
        //       style: TextStyle(
        //           fontSize: 32,
        //           fontWeight: FontWeight.bold,
        //           color: BOTTOMBAR_SELECTED_COLOR)),
        // ),
      ),
    );
  }
}
