import 'package:flutter/material.dart';
import '../class/custom_state_full_widget.dart';
import '../controller/bottombar_controller.dart';
import '../model/routes.dart';
import '../utils/theme.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  bool isFirstWidgetShow = true;
  var firstWidget = Routes.routes[0].widget;
  var secondWidget = Routes.routes[3].widget;
  var thirdWidget = Routes.routes[1].widget;

  @override
  void initState() {
    super.initState();
    firstWidget = Routes.routes[0].widget;
    secondWidget = Routes.routes[1].widget;
    thirdWidget = Routes.routes[3].widget;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BK_APP,
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          children: [
            // Padding Top pour tenir compte de la hauteur de barre de status
            // Padding(
            //     padding:
            //         EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            //     child: null),
            // Fade animation lors du changement de contenu central
            AnimatedCrossFade(
                firstChild: firstWidget,
                secondChild: secondWidget,
                crossFadeState: isFirstWidgetShow
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: const Duration(milliseconds: 300))
          ],
        )),
      ),
      bottomNavigationBar: SizedBox(
          height: BottombarController.height,
          child: BottombarController(refresh: updateMainContent)),
    );
  }

  // met à jour le contenu central en fonction de la route
  // renvoyée par la bottom navigation bar
  // le tableau des routes se trouve dans le fichier model/routes.dart
  void updateMainContent(int indexRoute) {
    // On vérifie si le widget est monté pour éviter les appels trop tôt
    if (!mounted) return;
    // Permet de mettre à jour le premier et deuxième widget pour l'animation de fade
    // en fonction de la route renvoyé par la bottomBar
    if (isFirstWidgetShow) {
      secondWidget = Routes.routes[indexRoute].widget;
      setState(() {
        isFirstWidgetShow = false;
      });
    } else {
      firstWidget = Routes.routes[indexRoute].widget;
      setState(() {
        isFirstWidgetShow = true;
      });
    }

    // Si le widget est un CustomStateFullWidget, on lance la méthode refresh
    if (Routes.routes[indexRoute].widget is CustomStateFullWidget) {
      CustomStateFullWidget? newWidget = Routes.routes[indexRoute].widget as CustomStateFullWidget?;
      newWidget?.refresh();
    }
  }
}
