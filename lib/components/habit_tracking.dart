import 'dart:ui';

import 'package:flutter/material.dart';

import '../model/habit.dart';
import '../model/occurence.dart';
import '../database/habits_database.dart';
import '../database/occurence_database.dart';
import '../model/valid_enum.dart';
import 'dart:math' as math;

import '../widget/agenda/habit_calendar_data_source.dart';

class StatsObject {
  var value;
  var all;

  StatsObject(value, all) {
    this.value = value ?? 0;
    this.all = all ?? all;
  }
}

/* ------------------- fonction pour les statistiques ------------------- */
class HabitTracking {
  final HabitsDatabase habitsDatabase = HabitsDatabase();
  final OccurenceDatabase occurenceDatabase = OccurenceDatabase();

  ///@param id_habit
  ///@return all occurences of an habit
  Future<List<Occurence>> getHabitById(int idHabit) async {
    return await occurenceDatabase.readAllByIdHabit(idHabit);
  }

  ///@return all Habits with their occurences
  Future<Map<Habit, List<Occurence>>> getAllHabit() async {
    return {
      for (var item in await habitsDatabase.readAllHabits())
        item: await getHabitById(item.getId())
    };
  }

  ///@return all habits that are in progress at this time
  Future<Map<Habit, List<Occurence>>> getHabitOnProgress() async {
    final list = await getAllHabit();
    Map<Habit, List<Occurence>> out = {};
    list.forEach((key, value) {
      if (key.getDateStart().isBefore(DateTime.now()) &&
          key.getDateEnd().isAfter(DateTime.now())) {
        out[key] = value;
      }
    });
    return out;
  }

  ///@param id_habit, date of starter, date of end
  ///@eturn number of habit per month - Map<month year, double>
  Future<Map<DateTime, double>> getNbHabitPerMonth() async {
    final list = await getAllHabit();
    if (list.isEmpty) return {};
    Map<DateTime, double> out = {};
    Map<DateTime, List<bool>> res = {};
    list.forEach((key, value) {
      for (var v in value) {
        if (res.containsKey(DateTime(v.getDate().year, v.getDate().month))) {
          res[DateTime(v.getDate().year, v.getDate().month)]?.add(true);
        } else {
          res[DateTime(v.getDate().year, v.getDate().month)] = [true];
        }
      }
    });

    res.forEach((key, value) {
      out[key] = ((value.where((element) => element).length).toDouble());
    });
    print("getNbHabitPerMonth");
    print(out);
    return out;
  }

  ///@param id_habit
  ///@return the success percentage for a habit
  Future<double> getSuccessRatePercentByHabit(idHabit) async {
    final list = await getHabitById(idHabit);
    if (list.isEmpty) return 0;
    final success =
        list.where((element) => element.getValidState() == ValidEnum.YES);
    return success.length / list.length;
  }

  ///@param id_habit
  ///@return the success statistique object for a habit
  Future<StatsObject> getSuccessRateByHabit(idHabit) async {
    final list = await getHabitById(idHabit);
    if (list.isEmpty) return StatsObject(0, 0);
    final success =
        list.where((element) => element.getValidState() == ValidEnum.YES);
    return StatsObject(success, list.length);
  }

  ///@param id_habit
  ///@return the failure percentage for a habit
  Future<double> getFailureRatePercentByHabit(idHabit) async {
    final list = await getHabitById(idHabit);
    if (list.isEmpty) return 0;
    final failure =
        list.where((element) => !(element.getValidState() == ValidEnum.YES));
    return failure.length / list.length;
  }

  ///@param id_habit
  ///@return the failure statistique object for a habit
  Future<StatsObject> getFailureRateByHabit(idHabit) async {
    final list = await getHabitById(idHabit);
    if (list.isEmpty) return StatsObject(0, 0);
    final failure =
        list.where((element) => !(element.getValidState() == ValidEnum.YES));
    return StatsObject(failure, list.length);
  }

  ///@param id_habit, date of starter, date of end
  ///@eturn the success percentage for a habit per month - Map<month year, percent>
  Future<Map<DateTime, double>> getSuccessRatePercentPerMonthByHabit(
      idHabit) async {
    final list = await getHabitById(idHabit);
    if (list.isEmpty) return {};
    Map<DateTime, double> out = {};
    Map<DateTime, List<bool>> res = {};
    for (var v in list) {
      if (res.containsKey(DateTime(v.getDate().year, v.getDate().month))) {
        res[DateTime(v.getDate().year, v.getDate().month)]
            ?.add(v.getValidState() == ValidEnum.YES);
      } else {
        res[DateTime(v.getDate().year, v.getDate().month)] = [
          v.getValidState() == ValidEnum.YES
        ];
      }
    }
    res.forEach((key, value) {
      out[key] = ((value.where((element) => element).length) / value.length);
    });
    return out;
  }

  Future<List<HabitudeCalendar>> getAllHabitPerMonth(
      int month, int year) async {
    List<HabitudeCalendar> list = <HabitudeCalendar>[];
    for (var item in await occurenceDatabase.readAllOccurences()) {
      if (item.date.month == month) {
        var habit = await habitsDatabase.readHabit(item.idHabit);
        list.add(HabitudeCalendar(
            habit.title,
            DateTime(item.date.year, item.date.month, item.date.day,
                habit.startDate.hour, habit.startDate.minute),
            DateTime(item.date.year, item.date.month, item.date.day,
                habit.endDate.hour, habit.endDate.minute),
            Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
                .withOpacity(1.0),
            false));
      }
    }
    return list;
  }

  ///@param id_habit, date of starter, date of end
  ///@eturn the success percentage per month - Map<month year, percent>
  Future<Map<DateTime, double>> getSuccessRatePercentPerMonth() async {
    final list = await getAllHabit();
    if (list.isEmpty) return {};
    Map<DateTime, double> out = {};
    Map<DateTime, List<bool>> res = {};
    list.forEach((key, value) {
      for (var v in value) {
        if (res.containsKey(DateTime(v.getDate().year, v.getDate().month))) {
          res[DateTime(v.getDate().year, v.getDate().month)]
              ?.add(v.getValidState() == ValidEnum.YES);
        } else {
          res[DateTime(v.getDate().year, v.getDate().month)] = [
            v.getValidState() == ValidEnum.YES
          ];
        }
      }
    });

    res.forEach((key, value) {
      out[key] = ((value.where((element) => element).length) / value.length);
    });
    return out;
  }

  ///@param id_habit, date of starter, date of end
  ///@eturn the success per month - Map<month year, double>
  Future<Map<DateTime, double>> getSuccessRatePerMonth() async {
    final list = await getAllHabit();
    if (list.isEmpty) return {};
    Map<DateTime, double> out = {};
    Map<DateTime, List<bool>> res = {};
    list.forEach((key, value) {
      for (var v in value) {
        if (res.containsKey(DateTime(v.getDate().year, v.getDate().month))) {
          res[DateTime(v.getDate().year, v.getDate().month)]
              ?.add(v.getValidState() == ValidEnum.YES);
        } else {
          res[DateTime(v.getDate().year, v.getDate().month)] = [
            v.getValidState() == ValidEnum.YES
          ];
        }
      }
    });

    res.forEach((key, value) {
      out[key] = ((value.where((element) => element).length).toDouble());
    });
    print(out);
    return out;
  }

  ///@param id_habit, date of starter, date of end
  ///@eturn the success statistique object for a habit per month - Map<month year, StatObject>
  Future<Map<DateTime, StatsObject>> getSuccessRatePerMonthByHabit(
      idHabit) async {
    final list = await getHabitById(idHabit);
    if (list.isEmpty) return {};
    Map<DateTime, StatsObject> out = {};
    Map<DateTime, List<bool>> res = {};
    for (var v in list) {
      if (res.containsKey(DateTime(v.getDate().year, v.getDate().month))) {
        res[DateTime(v.getDate().year, v.getDate().month)]
            ?.add(v.getValidState() == ValidEnum.YES);
      } else {
        res[DateTime(v.getDate().year, v.getDate().month)] = [
          v.getValidState() == ValidEnum.YES
        ];
      }
    }
    res.forEach((key, value) {
      out[key] =
          StatsObject((value.where((element) => element).length), value.length);
    });
    return out;
  }

  ///@param id_habit
  ///@eturn the failure percentage for a habit per month - Map<month year, percent>
  Future<Map<DateTime, double>> getFailureRatePercentPerMonth(idHabit) async {
    final list = await getHabitById(idHabit);
    if (list.isEmpty) return {};
    Map<DateTime, double> out = {};
    Map<DateTime, List<bool>> res = {};
    for (var v in list) {
      if (res.containsKey(DateTime(v.getDate().year, v.getDate().month))) {
        res[DateTime(v.getDate().year, v.getDate().month)]
            ?.add(v.getValidState() != ValidEnum.YES);
      } else {
        res[DateTime(v.getDate().year, v.getDate().month)] = [
          v.getValidState() != ValidEnum.YES
        ];
      }
    }
    res.forEach((key, value) {
      out[key] = ((value.where((element) => element).length) / value.length);
    });
    return out;
  }

  ///@param id_habit, date of starter, date of end
  ///@eturn the success statistique object for a habit per month - Map<month year, StatObject>
  Future<Map<DateTime, double>> getFailuresRatePerMonth() async {
    final list = await getAllHabit();

    Map<DateTime, double> out = {};
    Map<DateTime, List<bool>> res = {};
    list.forEach((key, value) {
      for (var v in value) {
        if (res.containsKey(DateTime(v.getDate().year, v.getDate().month))) {
          res[DateTime(v.getDate().year, v.getDate().month)]
              ?.add(v.getValidState() != ValidEnum.YES);
        } else {
          res[DateTime(v.getDate().year, v.getDate().month)] = [
            v.getValidState() != ValidEnum.YES
          ];
        }
      }
    });

    res.forEach((key, value) {
      out[key] = ((value.where((element) => element).length).toDouble());
    });
    return out;
  }
}
