// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

const Color BK_APP = Color(0xFF161719);
const Color ACCENT_COLOR = Color(0xffc932d9);

// const Color BOTTOMBAR_BK = Colors.purple;
const Color BOTTOMBAR_BK = Color(0xFF161719);
const Color BOTTOMBAR_TOP_LINE = Color.fromARGB(255, 94, 99, 107);
const Color BOTTOMBAR_SELECTED_COLOR = Color.fromARGB(255, 85, 136, 212);

// const Color BK_APP = Colors.white;
const Color TEXT_COLOR = Colors.white;
const Color ICON_COLOR = Colors.white;

const Color BK_CARD_1 = Color(0xff272845);
const Color BK_CARD_2 = Color(0xff323052);

const Color TITLE_CARD = Colors.white;
const Color DESCRIPTION_CARD = Color(0xff9293bf);

const Color CHECK_HABIT = Color.fromARGB(255, 30, 163, 134);
const Color NO_CHECK_HABIT = Color.fromARGB(255, 182, 34, 91);
const Color CHECK_WAITING = Colors.grey;
const Color SUB_TITLE = Colors.grey;
const Color BUTTON_ADD_HABIT = Color.fromARGB(255, 182, 34, 91);

// WeekBar
const Color WEEKBAR_DAY_COLOR = Color(0xff272845);
const Color WEEKBAR_SELECTED_DAY_BK = Color.fromARGB(255, 77, 73, 126);
const Color WEEKBAR_BK = Color(0xFF161719);
// const WEEKBAR_LINEAR_BK = [
//   Color.fromARGB(255, 122, 0, 163),
//   Color(0xffc932d9),
// ];
const Color WEEKBAR_ICON_COLOR = Color.fromARGB(255, 223, 211, 255);

// Circle percent widget
const Color CIRCLE_TITLE = Color(0xFFC3C8D9);
const Color CIRCLE_TEXT = Color(0xFFDCE2F5);
const Color CIRCLE_COLOR0 = Color(0xFF8DE4CA);
const Color CIRCLE_COLOR1 = Color(0xFF509983);
