import 'package:flutter/material.dart';
import 'package:levelup/model/routes.dart';

import '../utils/theme.dart';

class BottombarController extends StatefulWidget {
  const BottombarController({Key? key, required this.refresh})
      : super(key: key);

  final Function(int indexRoute) refresh;
  static const double height = 50;
  @override
  BottombarControllerState createState() => BottombarControllerState();
}

class BottombarControllerState extends State<BottombarController> {
  @override
  void initState() {
    super.initState();
  }

  int _selectedIndex = 0;
  void onItemTapped(int index) {
    if (_selectedIndex != index) {
      setState(() {
        _selectedIndex = index;
        widget.refresh(_selectedIndex);
      });
    }
  }

  int getSelectedIndex() {
    int index = _selectedIndex;
    return index;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        BottomNavigationBar(
          iconSize: 16,
          items: <BottomNavigationBarItem>[
            for (var route in Routes.routes)
              BottomNavigationBarItem(
                icon: route.icon,
                label: route.name,
                backgroundColor: route.backgroundColor,
              ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: BOTTOMBAR_SELECTED_COLOR,
          onTap: onItemTapped,
        ),
        Container(
          height: 1,
          color: BOTTOMBAR_TOP_LINE,
        ),
      ],
    );
  }
}
