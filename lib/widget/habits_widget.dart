import 'package:flutter/cupertino.dart';
import 'package:levelup/widget/habits/create.dart';
import 'package:levelup/widget/habits/details.dart';
import 'package:levelup/widget/habits/edit.dart';
import 'package:levelup/widget/habits/list.dart';

class Habits extends StatefulWidget {
  const Habits({Key? key}) : super(key: key);

  @override
  _HabitsState createState() => _HabitsState();
}

enum HabitSubPages { list, create, details, edit }

class _HabitsState extends State<Habits> {
  Widget? _currentWidget;

  void changeSubPage({required HabitSubPages pageName, int? id}) {
    Widget? result;

    switch (pageName) {
      case HabitSubPages.list:
        result = HabitList(callFunction: changeSubPage);
        break;
      case HabitSubPages.create:
        result = HabitCreateForm(callFunction: changeSubPage);
        break;
      case HabitSubPages.edit:
        result = HabitEditWidget(callFunction: changeSubPage, id: id);
        break;
      case HabitSubPages.details:
        result = HabitDetails(callFunction: changeSubPage, id: id);
        break;
      default:
        result = HabitList(callFunction: changeSubPage);
        break;
    }
    if (mounted) {
      setState(() {
        _currentWidget = result!;
        print("Habits change widget");
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // On charge la première page au début
    changeSubPage(pageName: HabitSubPages.list);
    print("initState Habits");
  }

  @override
  Widget build(BuildContext context) {
    return _currentWidget ?? const SizedBox.shrink();
  }
}
