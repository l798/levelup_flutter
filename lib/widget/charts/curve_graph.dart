import 'package:flutter/material.dart';

import 'chart.dart';
import 'chart_background_painter.dart';
import 'chart_painter.dart';
import 'interact_notification.dart';
import 'scaling_info.dart';

class CurveGraph extends StatefulWidget {
  final Chart chart;

  const CurveGraph({
    Key? key,
    required this.chart,
  }) : super(key: key);

  @override
  State createState() => _CurveGraphState();
}

class _CurveGraphState extends State<CurveGraph>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late double _startRange;

  @override
  void initState() {
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 600), value: 1.0);
    _controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(context) {
    ScalingInfo.init(context);
    const textStyle = TextStyle(
      color: Color(0xFFC4C8D9),
      fontSize: 12,
      fontFamily: 'Lato',
      fontWeight: FontWeight.w200,
    );
    const labelStyle = TextStyle(
      color: Color(0xFFDCE2F5),
      fontFamily: 'Lato',
      fontSize: 10,
    );
    final List<String> yAxisLabels = [
      '20',
      '15',
      '10',
      '5',
      '0',
    ];
    String label0Text = "", label1Text = "";
    double label0Y = 0.0, label1Y = 0.0;
    if (widget.chart.selectedDataPoint != -1) {
      label0Text = widget
          .chart.dataSets[0].values[widget.chart.selectedDataPoint]
          .toString();
      label1Text = widget
          .chart.dataSets[1].values[widget.chart.selectedDataPoint]
          .toString();
      label0Y =
          150 * ScalingInfo.scaleY * (1.0 - widget.chart.selectedY(0)) + 10;
      label1Y =
          150 * ScalingInfo.scaleY * (1.0 - widget.chart.selectedY(1)) + 10;
      // Resolve label intersection
      final d = label1Y - label0Y;
      if (d.abs() < 12) {
        label1Y += 24;
      }
    }

    final appSize = MediaQuery.of(context).size;

    return GestureDetector(
      onTapUp: (details) => _handleTap(appSize, details),
      onHorizontalDragStart: (_) => _handleStartInteract(context),
      onHorizontalDragUpdate: _handleDrag,
      onHorizontalDragEnd: (_) => _handleEndInteract(context),
      onScaleStart: (details) {
        _handleStartInteract(context);
        _handleStartZoom(details);
      },
      onScaleUpdate: _handleZoom,
      onScaleEnd: (_) => _handleEndInteract(context),
      child: SizedBox(
        //width: 250 * ScalingInfo.scaleX,
        height: 160 * ScalingInfo.scaleY,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            SizedBox(
              width: appSize.width,
              height: 150 * ScalingInfo.scaleY,
              child: CustomPaint(
                  painter: ChartBackgroundPainter(widget.chart),
                  foregroundPainter: ChartPainter(
                    widget.chart,
                    _controller,
                    [
                      const Color(0x4CDEACD0),
                      const Color(0x00DEACD0),
                      const Color(0x4C4AC3E5),
                      const Color(0x005290C7),
                    ],
                    [
                      const Color(0xFFA74CBA),
                      const Color(0xFFF287A6),
                      const Color(0xFF4A78ED),
                      const Color(0xFF5DB391),
                    ],
                  )),
            ),
            Positioned(
              left: 4,
              height: 150 * ScalingInfo.scaleY,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: yAxisLabels
                    .map((label) => Text(label, style: textStyle))
                    .toList(),
              ),
            ),
            Positioned(
              left: appSize.width - 24,
              height: 150 * ScalingInfo.scaleY,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: yAxisLabels
                    .map((label) => Text(label, style: textStyle))
                    .toList(),
              ),
            ),
            if (widget.chart.selectedDataPoint != -1) ...{
              _buildLabel(appSize, label0Text, labelStyle, label0Y),
              _buildLabel(appSize, label1Text, labelStyle, label1Y),
            },
          ],
        ),
      ),
    );
  }

  Widget _buildLabel(Size appSize, String text, TextStyle style, double y) {
    return Positioned(
      left: appSize.width * widget.chart.selectedX() + 8,
      top: y,
      width: 40,
      child: Container(
        color: const Color(0x99252B40)
            .withOpacity(_controller.value < 0.6 ? _controller.value : 0.6),
        padding: const EdgeInsets.all(4),
        child: Text(text,
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: const Color(0xFFDCE2F5).withOpacity(_controller.value))),
      ),
    );
  }

  void _handleTap(Size appSize, TapUpDetails details) {
    final x = (details.localPosition.dx - 28) / appSize.width;
    final offset = lerp(widget.chart.domainStart, widget.chart.domainEnd, x);
    if (offset.round() != widget.chart.selectedDataPoint) {
      widget.chart.selectedDataPoint = offset.round();
      _controller.forward(from: 0.0);
    }
  }

  void _handleStartInteract(BuildContext context) {
    InteractNotification(false).dispatch(context);
  }

  void _handleEndInteract(BuildContext context) {
    InteractNotification(true).dispatch(context);
  }

  void _handleStartZoom(ScaleStartDetails details) {
    _startRange = widget.chart.domainEnd - widget.chart.domainStart;
  }

  void _handleZoom(ScaleUpdateDetails details) {
    double d = 1.0 / details.scale;
    if (d == 0) return;
    final targetRange = _startRange * d;
    final range = widget.chart.domainEnd - widget.chart.domainStart;
    final scale = targetRange - range;

    if (range + scale < 13.0) {
      if (widget.chart.domainEnd != widget.chart.maxDomain) {
        widget.chart.domainEnd += scale;
      } else {
        widget.chart.domainStart -= scale;
      }
    }

    _controller.reverse();
  }

  void _handleDrag(DragUpdateDetails details) {
    final d = -details.primaryDelta! /
        200 *
        (widget.chart.domainEnd - widget.chart.domainStart);
    if (widget.chart.domainStart + d < 0 ||
        widget.chart.domainEnd + d >= widget.chart.maxDomain) return;
    if (d < 0) {
      widget.chart.domainStart += d;
      widget.chart.domainEnd += d;
    } else {
      widget.chart.domainEnd += d;
      widget.chart.domainStart += d;
    }
    _controller.reverse();
  }

  double lerp(double x, double y, double s) {
    return x * (1 - s) + y * s;
  }
}
