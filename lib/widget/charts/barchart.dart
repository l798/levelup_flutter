import 'package:flutter/material.dart';
import 'dart:math' as math;

class Barchart extends StatefulWidget {
  const Barchart(
      {Key? key,
      required this.height,
      required this.values,
      this.width,
      this.backGroundColor = Colors.white,
      this.lineColor = Colors.black87,
      this.xColor = Colors.black87,
      this.yColor = Colors.black87})
      : super(key: key);

  final List<double> values;
  final double height;
  final double? width;
  final Color backGroundColor;
  final Color lineColor;
  final Color xColor;
  final Color yColor;
  @override
  _BarchartState createState() => _BarchartState();
}

class _BarchartState extends State<Barchart> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // set max Width
    double maxWidth = widget.width ??
        math.min(MediaQuery.of(context).size.width,
            widget.width ?? MediaQuery.of(context).size.width);
    return Center(
        child: Container(
            width: maxWidth,
            height: widget.height,
            color: widget.backGroundColor,
            child: getRowBar()));
  }

  Row getRowBar() {
    List<Widget> children = [];
    for (var item in widget.values) {
      {
        children.add(Padding(
            padding: const EdgeInsets.only(right: 10),
            child: barGraph(
                width: 50, height: item, text: item.toStringAsFixed(1))));
      }
    }
    return Row(crossAxisAlignment: CrossAxisAlignment.end, children: children);
  }

  AnimatedContainer barGraph(
      {required double width,
      required double height,
      required String text,
      Color color = const Color(0xFF1565C0),
      int duration = 1000}) {
    return AnimatedContainer(
      width: width,
      height: height,
      color: color,
      curve: Curves.linear,
      duration: Duration(milliseconds: duration),
      child: Center(
        child: Text(text),
      ),
    );
  }
}
