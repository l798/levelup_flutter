import 'package:flutter/material.dart';

import '../../utils/theme.dart';
import 'circle_percentage_painter.dart';

class CirclePercentageWidget extends StatefulWidget {
  final String title;
  final double percent;
  final Color color0 = CIRCLE_COLOR0;
  final Color color1 = CIRCLE_COLOR1;

  CirclePercentageWidget({Key? key, required this.title, required this.percent})
      : super(key: key);

  @override
  State createState() => _CirclePercentageWidgetState();
}

class _CirclePercentageWidgetState extends State<CirclePercentageWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2400));

    _controller.addListener(() {
      setState(() {});
    });

    _controller.animateTo(widget.percent);

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(CirclePercentageWidget oldWidget) {
    if (oldWidget.percent != widget.percent) {
      _controller.animateTo(widget.percent);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(context) {
    /*
    final titleStyle = TextStyle(
      color: CIRCLE_TITLE,
      fontFamily: 'Noto',
      fontWeight: FontWeight.w200,
      fontSize: 14,
    );

    final labelStyle = TextStyle(
      color: CIRCLE_TEXT,
      fontFamily: 'Noto',
      fontWeight: FontWeight.w200,
      fontSize: 14,
    );*/

    return Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Container(
          width: 150,
          height: 75,
          alignment: Alignment.center,
          child: Text(widget.title,
              textAlign: TextAlign.center,
              style: TextStyle(color: CIRCLE_TITLE, fontSize: 16))),
      Container(
        width: 42 * ((MediaQuery.of(context).size.width) / 320),
        height: 42 * ((MediaQuery.of(context).size.height) / 480),
        margin: const EdgeInsets.all(12),
        child: CustomPaint(
          isComplex: false,
          painter: SpendingCategoryChartPainter(
              _controller.value, context, widget.color0, widget.color1),
          child: Center(
              child: Text('${(_controller.value * 100).toInt()}%',
                  style: TextStyle(color: CIRCLE_TEXT, fontSize: 14))),
        ),
      ),
    ]);
  }
}
