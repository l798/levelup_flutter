import 'chart_data_set.dart';

class ChartData {
  ChartDataSet dataSet1 = ChartDataSet([
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
    8.0,
    9.0,
    10.0,
    7.0,
    7.1,
    6.8,
    6.8,
    7.0,
    7.0,
    7.0,
    7.1,
    7.1,
    7.2,
    7.2
  ]);

  ChartDataSet dataSet2 = ChartDataSet([
    5.8,
    5.0,
    5.0,
    4.5,
    4.6,
    4.7,
    5.6,
    5.2,
    4.6,
    2.1,
    4.6,
    4.7,
    4.6,
    4.5,
    4.5,
    5.2,
    5.1,
    5.0,
    5.0,
    4.6,
    4.7
  ]);
}
