import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:levelup/model/habit.dart';
import 'package:levelup/model/occurence.dart';
import 'package:levelup/model/valid_enum.dart';

import '../../utils/theme.dart';

/// This class show habit card for view in day
// ignore: must_be_immutable
class WeekOccurenceCard extends StatefulWidget {
  const WeekOccurenceCard(
      {Key? key,
      required this.habit,
      required this.occurence,
      required this.isEven})
      : super(key: key);
  final Habit habit;
  final Occurence occurence;
  final bool isEven;

  @override
  State<WeekOccurenceCard> createState() => _WeekOccurenceCardState();
}

class _WeekOccurenceCardState extends State<WeekOccurenceCard> {
  Occurence? occurence;

  @override
  initState() {
    super.initState();
    if (mounted && widget.habit.id != null) {
      setState(() {
        occurence = widget.occurence;
      });
    }
  }

  Future changeValid() async {
    widget.occurence.valid =
        widget.occurence.valid == ValidEnum.YES ? ValidEnum.NO : ValidEnum.YES;
    var result = await widget.habit.occurenceDB.update(widget.occurence);
    setState(() {
      occurence = widget.occurence;
    });
    print("update occurence");
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      occurence = widget.occurence;
    });
    return Container(
      height: 100,
      width: 300,
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
      decoration: BoxDecoration(
        color: widget.isEven ? BK_CARD_1 : BK_CARD_2,
        boxShadow: [
          BoxShadow(
            blurRadius: 4.0,
            color: Colors.black.withOpacity(0.5),
          )
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  widget.habit.title,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      letterSpacing: .3),
                ),
              ),
              Text(
                '${DateFormat('H').format(widget.habit.startDate)}h',
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    letterSpacing: .3),
              ),
            ],
          ),
          const SizedBox(height: 2.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Text(
                  widget.habit.description,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontSize: 13, letterSpacing: .3, color: DESCRIPTION_CARD),
                ),
              ),
              InkWell(
                onTap: () => {changeValid()},
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Icon(
                    Icons.check_circle_outline,
                    size: 24.0,
                    // color: (occurence?.getValidState() == ValidEnum.YES)
                    color: (occurence?.getValidState() == ValidEnum.YES)
                        ? CHECK_HABIT
                        : (occurence?.getValidState() == ValidEnum.NO)
                            ? NO_CHECK_HABIT
                            : CHECK_WAITING,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
