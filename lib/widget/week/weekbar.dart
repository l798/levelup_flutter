import 'package:flutter/material.dart';

import '../../utils/theme.dart';

// Widget qui affiche une barre avec les jours de la semaine courante
class WeekBar extends StatefulWidget {
  const WeekBar(
      {Key? key, required this.context, required this.sendSelectedDay})
      : super(key: key);
  final BuildContext context;
  // fonction qui sera appelé à la sélection d'un jour
  // Permet d'appeler la fonction du parent passé en paramètre
  final Function(DateTime selectedDay) sendSelectedDay;
  // Constante de hauteur de la weekbar
  static const double height = 80;
  @override
  _WeekbarWidgetState createState() => _WeekbarWidgetState();
}

// on ajoute SingleTickerProviderStateMixin au state pour mieux gérer les animations
class _WeekbarWidgetState extends State<WeekBar>
    with SingleTickerProviderStateMixin {
  List<String> dayInWeek = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
  int selectedDay = DateTime.now().weekday - 1;
  late AnimationController _animationController;
  final Duration _duration = const Duration(milliseconds: 100);
  late CurvedAnimation _curvedAnimation;
  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(duration: _duration, vsync: this);
    _curvedAnimation =
        CurvedAnimation(parent: _animationController, curve: Curves.easeIn);
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 8.0, bottom: 1.0),
        color: WEEKBAR_BK,
        // decoration: const BoxDecoration(color: WEEKBAR_BK
        //     // gradient: LinearGradient(
        //     //   begin: Alignment.topCenter,
        //     //   end: Alignment.bottomCenter,
        //     //   stops: [0.9, 1],
        //     //   colors: WEEKBAR_LINEAR_BK,
        //     // ),
        //     ),
        child: getRadioDays(dayInWeek, selectedDay));
  }

  // Renvoie une ligne de boutons de jour
  Widget getRadioDays(List<String> days, int selectedState) {
    List<Widget> list = [];
    var currentDate = DateTime.now();
    var numDayInWeek = currentDate.weekday - 1;
    for (var i = 0; i < days.length; i++) {
      // On déduit le numéro de jour pour tous les boutons
      int dayDate = currentDate.day - (numDayInWeek - i);
      list.add(radioDay(days[i], i, selectedState,
          DateTime(currentDate.year, currentDate.month, dayDate)));
    }
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: list);
  }

  void changeDaySelected(int index) {
    setState(() {
      selectedDay = index;
    });
  }

  /// bouton radio personnalisé
  /// name -> texte du bouton radio
  /// value -> son index dans le groupe de bouton radioDay
  /// selected -> le state qui sera modifié lorsque le bouton sera sélectionné
  /// date -> la date du jour représenté par le bouton radio
  Widget radioDay(String name, int value, int selected, DateTime date) {
    double widthScreen = MediaQuery.of(widget.context).size.width;
    return Stack(alignment: AlignmentDirectional.topCenter, children: [
      SizedBox(
        width: widthScreen / 7.5,
        height: WeekBar.height - 10,
        child: InkWell(
          child: DecoratedBoxTransition(
            decoration: DecorationTween(
                    begin: _animatedShadow(selected, value, 0),
                    end: _animatedShadow(selected, value, -3.0))
                .animate(_animationController),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(name,
                  style: widthScreen > 400
                      ? const TextStyle(fontSize: 18)
                      : const TextStyle(fontSize: 13)),
              Text(date.day.toString(),
                  style: widthScreen > 400
                      ? const TextStyle(fontSize: 20)
                      : const TextStyle(fontSize: 16)),
              AnimatedContainer(
                  duration: _duration,
                  height: selected == value ? 16 : 0,
                  //color: Colors.amberAccent,
                  child: ScaleTransition(
                      scale: Tween<double>(begin: 0, end: 1)
                          .animate(_curvedAnimation),
                      child: null))
            ]),
          ),
          onTap: () {
            if (selectedDay != value) {
              changeDaySelected(value);
              widget.sendSelectedDay(date);
              // Il faut réinitialiser l'animation pour éviter des artefacts
              _animationController.reset();
              _animationController.forward();
            }
          },
        ),
      ),
      Transform.translate(
        offset: const Offset(0.0, -4.0),
        child: Container(
          alignment: Alignment.bottomCenter,
          height: WeekBar.height,
          child: ScaleTransition(
              scale: Tween<double>(begin: 0, end: 1).animate(_curvedAnimation),
              child: selected == value
                  ? const Icon(Icons.today_rounded,
                      color: WEEKBAR_ICON_COLOR, size: 24)
                  : null),
        ),
      )
    ]);
  }

  BoxDecoration _animatedShadow(selected, value, double spread) {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: selected != value ? WEEKBAR_DAY_COLOR : null,
      boxShadow: selected == value
          ? [
              const BoxShadow(color: WEEKBAR_DAY_COLOR),
              BoxShadow(
                color: WEEKBAR_SELECTED_DAY_BK,
                blurRadius: 2.5,
                spreadRadius: spread,
              ),
            ]
          : null,
    );
  }
}
