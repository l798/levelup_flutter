// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:levelup/class/custom_state_full_widget.dart';
import 'package:levelup/model/day_occurences.dart';
import 'package:levelup/model/habit.dart';
import 'package:levelup/controller/bottombar_controller.dart';
import 'package:levelup/database/habits_database.dart';
import 'package:levelup/widget/week/week_occurence_card.dart';
import 'package:levelup/widget/week/weekbar.dart';

import '../../utils/theme.dart';

// Classe qui étends du CustomStateFullWidget nous permettant
// de définir une méthode appelée par le parent
// Nous avons utilisé ce moyen pour mettre à jour en revenant sur le widget
class WeekComponent extends CustomStateFullWidget {
  WeekComponent({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _WeekComponentState();

  @override
  void refresh() {
    stateMethod();
  }
}

class _WeekComponentState extends State<WeekComponent> {
  List<Habit> habits = [];
  List<DayOccurences> dayOccurences = [];
  DateTime stateDate = DateTime.now();

  @override
  initState() {
    super.initState();
    widget.stateMethod = requestRefresh;
    setState(() {
      stateDate = DateTime(stateDate.year, stateDate.month, stateDate.day);
    });
    getHabitsByDate(stateDate);
  }

  // Appelée quand on change de jour sur la weekbar
  void updateDate(DateTime date) {
    getHabitsByDate(date);
  }

  // Appelée par le parent du widget
  void requestRefresh() {
    getHabitsByDate(stateDate);
  }

  /// Renvoie les habitudes et les occurences pour une date donnée */
  Future getHabitsByDate(DateTime date) async {
    var listHabits = await HabitsDatabase().getHabitsByDay(date);
    // Pour chaque habitude on récupère les occurences du jour
    List<DayOccurences> occs = [];
    for (var element in listHabits) {
      var resultOccurence = await getOccurencesByHabit(element, date);
      occs.addAll(resultOccurence
          .map((e) => DayOccurences(element, e))
          .cast<DayOccurences>()
          .toList());
    }

    // trie des occurences par date pour l'affichage vertical
    occs.sort((a, b) => a.occurence.date.compareTo(b.occurence.date));
    // Mise à jour du state
    if (mounted == true) {
      setState(() {
        habits = listHabits;
        stateDate = date;
        dayOccurences = occs;
      });
    }
  }

  /// Renvoie toutes les occurences pour une habitude à une date donnée */
  Future getOccurencesByHabit(Habit habit, DateTime date) async {
    if (habit.id != null) {
      var occurences = await habit.occurenceDB
          .readOccurencesBetweenDateByIdHabit(habit.id!, date, null);
      return occurences;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      const ListTile(
        leading: Image(
          image: AssetImage("assets/habits_logo.png"),
          width: 40,
          height: 40,
        ),
        title: Text(
          "Ma semaine",
          style: TextStyle(color: TEXT_COLOR, fontWeight: FontWeight.bold),
        ),
      ),
      WeekBar(context: context, sendSelectedDay: updateDate),
      SizedBox(
        height: MediaQuery.of(context).size.height -
            (BottombarController.height + WeekBar.height + 33),
        child: ListView.builder(
            padding: const EdgeInsets.only(top: 0),
            itemCount: dayOccurences.length,
            itemBuilder: (context, index) {
              return WeekOccurenceCard(
                habit: dayOccurences[index].habit,
                occurence: dayOccurences[index].occurence,
                isEven: index.isEven,
              );
            }),
      ),
    ]);
  }
}
