// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:levelup/class/custom_state_full_widget.dart';
import 'package:levelup/widget/charts/circle_percentage_widget.dart';
import '../components/habit_tracking.dart';
import '../controller/bottombar_controller.dart';
import '../model/habit.dart';
import '../utils/theme.dart';
import 'charts/chart.dart';
import 'charts/chart_data_set.dart';
import 'charts/curve_graph.dart';
import 'charts/demo_chart_data.dart';
import 'charts/interact_notification.dart';

// Classe qui étends du CustomStateFullWidget nous permettant
// de définir une méthode appelée par le parent
// Nous avons utilisé ce moyen pour mettre à jour en revenant sur le widget
class StatisticalComponent extends CustomStateFullWidget {
  StatisticalComponent({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _StatisticalComponentState();

  @override
  void refresh() {
    stateMethod();
  }
}

class _StatisticalComponentState extends State<StatisticalComponent>
    with SingleTickerProviderStateMixin {
  Map<DateTime, double> habitsPerMonth = {};
  Map<Habit, double> successPerHabits = {};
  final tracking = HabitTracking();

  late AnimationController _controller;
  late Chart? _chart;

  @override
  initState() {
    widget.stateMethod = requestRefresh;
    _chart = null;
    initChartData();
    refreshGraph();
    super.initState();
  }

  void initChartData() async {
    HabitTracking ht = HabitTracking();
    var failurePerMonth = await ht.getFailuresRatePerMonth();
    var nbHabitPerMonth = await ht.getNbHabitPerMonth();
    ChartDataSet dataFailure =
        ChartDataSet([5, 2, 3, 8, 7, 9, 6, 1, 2, 12, 8, 5, 10]);
    ChartDataSet dataNbHabit =
        ChartDataSet([3, 2, 3, 5, 4, 5, 3, 1, 2, 15, 12, 7, 13]);
    // var data = ChartData();
    _chart = Chart([dataFailure, dataNbHabit], '', '\$');
    _chart!.domainStart = 0;
    _chart!.domainEnd = 6;
    _chart!.rangeStart = 0;
    _chart!.rangeEnd = 20;
    _chart!.selectedDataPoint = 1;
    _chart!.addListener(() => setState(() {}));

    _controller = AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 12000),
        upperBound: _chart!.maxDomain);

    _controller.addListener(() {
      final d = _controller.value - _chart!.domainStart;
      _chart!.domainStart += d;
      _chart!.domainEnd += d;
    });
  }

  void requestRefresh() {
    refreshGraph();
  }

  void refreshGraph() {
    tracking.getSuccessRatePercentPerMonth().then((value) {
      if (mounted) {
        setState(() {
          habitsPerMonth = value;
        });
      }
    });
    tracking.getAllHabit().then((list) {
      if (list.isNotEmpty) {
        successPerHabits = {};
        list.forEach((key, value) {
          tracking.getSuccessRatePercentByHabit(key.getId()).then((percent) {
            if (mounted) {
              setState(() {
                successPerHabits[key] = percent;
              });
            }
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - BottombarController.height,
      width: MediaQuery.of(context).size.width,
      child: Container(
        height: 10,
        width: MediaQuery.of(context).size.width - 12.0,
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        decoration: BoxDecoration(
          color: BK_CARD_1,
          boxShadow: [
            BoxShadow(
              blurRadius: 4.0,
              color: Colors.black.withOpacity(0.5),
            )
          ],
        ),
        child: Column(children: <Widget>[
          const ListTile(
            leading: Image(
              image: AssetImage("assets/statistics_logo.png"),
              width: 40,
              height: 40,
            ),
            title: Text(
              "Statistiques",
              style: TextStyle(color: TEXT_COLOR, fontWeight: FontWeight.bold),
            ),
          ),
          NotificationListener<InteractNotification>(
            onNotification: _handleInteract,
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Flexible(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          // ignore: unnecessary_null_comparison
                          if (_chart != null) CurveGraph(chart: _chart!),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          const Text("By Habit",
              style: TextStyle(
                  color: TEXT_COLOR,
                  fontSize: 16,
                  fontWeight: FontWeight.bold)),
          Container(
              margin: const EdgeInsets.symmetric(vertical: 20.0),
              height: 175,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: successPerHabits.entries.map((entry) {
                  return CirclePercentageWidget(
                      title: entry.key.title, percent: entry.value);
                }).toList(),
              ))
        ]),
      ),
    );
  }

  bool _handleInteract(InteractNotification notification) {
    if (notification.end) {
      _controller.value = _chart!.domainStart;
      double target = _chart!.domainStart.round().toDouble();
      _controller.animateTo(target, curve: Curves.easeIn);
    } else {
      //_controller.stop();
    }
    return false;
  }
}
