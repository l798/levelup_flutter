import 'package:flutter/material.dart';
import 'package:levelup/model/habit.dart';
import 'package:levelup/model/occurence.dart';
import 'package:levelup/widget/charts/barchart.dart';
import 'dart:math' as math;

import 'package:levelup/components/habit_tracking.dart';

class CalendrierWidget extends StatefulWidget {
  const CalendrierWidget({Key? key}) : super(key: key);

  @override
  _CalendrierWidget createState() => _CalendrierWidget();
}

class _CalendrierWidget extends State<CalendrierWidget> {
  List<double> values = [];

  HabitTracking tracking = HabitTracking();
  Map<Habit, List<Occurence>> valuesoccurences = {};
  @override
  void initState() {
    super.initState();
    refreshHabits();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      timing();
    });
  }

  Future refreshHabits() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Barchart(height: 300, values: values);
  }

  void timing() {
    var rnd = math.Random();
    Future.delayed(const Duration(milliseconds: 2000), () {
      setState(() {
        values = [
          rnd.nextDouble() * 200,
          rnd.nextDouble() * 200,
          rnd.nextDouble() * 200,
          rnd.nextDouble() * 200,
          rnd.nextDouble() * 200,
          rnd.nextDouble() * 200
        ];
      });
      timing();
    });
  }
}
