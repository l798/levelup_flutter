// ignore_for_file: must_be_immutable

import 'dart:core';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:levelup/components/habit_tracking.dart';
import 'package:levelup/model/habit.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:levelup/controller/bottombar_controller.dart';
import '../../class/custom_state_full_widget.dart';
import '../../utils/theme.dart';

import 'habit_calendar_data_source.dart';

class Agenda extends CustomStateFullWidget {
  Agenda({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AgendaState();

  @override
  void refresh() {
    stateMethod();
  }
}

class _AgendaState extends State<Agenda> {

  HabitTracking tracking = HabitTracking();
  HabitudeCalendarDataSource source = HabitudeCalendarDataSource(<HabitudeCalendar>[]);
  Widget _calendar = SfCalendar(
    view: CalendarView.month,
    timeSlotViewSettings: const TimeSlotViewSettings(timeIntervalHeight: 10),
    dataSource: HabitudeCalendarDataSource(<HabitudeCalendar>[]),
    monthViewSettings: const MonthViewSettings(
        showAgenda: true,
        agendaItemHeight: 70,
        agendaStyle: AgendaStyle(
          backgroundColor: BK_APP,
          appointmentTextStyle: TextStyle(fontSize: 14, color: TEXT_COLOR),
          dateTextStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w300, color: TEXT_COLOR),
          dayTextStyle: TextStyle(fontStyle: FontStyle.normal, fontSize: 20, fontWeight: FontWeight.w700, color: TEXT_COLOR),
        )),
    backgroundColor: BK_APP,
    monthCellBuilder: (BuildContext buildContext, MonthCellDetails details) {
      const Color backgroundColor = BK_CARD_1;
      final Color defaultColor = Colors.black54;
      return Container(
        decoration: BoxDecoration(
            color: backgroundColor,
            border: Border.all(color: BK_APP, width: 0.5)),
        child: Center(
          child: Text(details.date.day.toString(), style: const TextStyle(color: TEXT_COLOR),),
        ),);},
    cellBorderColor: BK_CARD_1,
    todayTextStyle: const TextStyle(color: TEXT_COLOR),
    todayHighlightColor: ACCENT_COLOR,
    weekNumberStyle: const WeekNumberStyle(
        backgroundColor: BK_CARD_1,
        textStyle: TextStyle(color: TEXT_COLOR)),
    headerStyle: const CalendarHeaderStyle(
        backgroundColor: BK_APP,
        textStyle: TextStyle(color: TEXT_COLOR)),
    blackoutDatesTextStyle: const TextStyle(color: TEXT_COLOR),
    appointmentTextStyle: const TextStyle(color: TEXT_COLOR),
    viewHeaderStyle: const ViewHeaderStyle(
        backgroundColor: BK_CARD_2,
        dayTextStyle: TextStyle(color: TEXT_COLOR)),

    //monthViewSettings: const MonthViewSettings(
    //appointmentDisplayMode: MonthAppointmentDisplayMode.appointment),
  );

  @override
  void initState() {
    super.initState();
    widget.stateMethod = refresh;
    refresh();
  }

  void refresh() {
    setState(() {
      if(mounted){
        tracking.getAllHabitPerMonth(DateTime.now().month, DateTime.now().month).then((value){
          source.appointments = value;
          _calendar = SfCalendar(
            view: CalendarView.month,
            timeSlotViewSettings: const TimeSlotViewSettings(timeIntervalHeight: 10),
            dataSource: source,
            monthViewSettings: const MonthViewSettings(
                showAgenda: true,
                agendaItemHeight: 70,
                agendaStyle: AgendaStyle(
                  backgroundColor: BK_APP,
                  appointmentTextStyle: TextStyle(fontSize: 14, color: TEXT_COLOR),
                  dateTextStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w300, color: TEXT_COLOR),
                  dayTextStyle: TextStyle(fontStyle: FontStyle.normal, fontSize: 20, fontWeight: FontWeight.w700, color: TEXT_COLOR),
                )),
            backgroundColor: BK_APP,
            monthCellBuilder: (BuildContext buildContext, MonthCellDetails details) {
              const Color backgroundColor = BK_CARD_1;
              final Color defaultColor = Theme.of(context).brightness == Brightness.dark ? Colors.black54 : TEXT_COLOR;
              return Container(
                decoration: BoxDecoration(
                    color: backgroundColor,
                    border: Border.all(color: BK_APP, width: 0.5)),
                child: Center(
                  child: Text(details.date.day.toString(), style: const TextStyle(color: TEXT_COLOR),),
                ),);},
            cellBorderColor: BK_CARD_1,
            todayTextStyle: const TextStyle(color: TEXT_COLOR),
            todayHighlightColor: ACCENT_COLOR,
            weekNumberStyle: const WeekNumberStyle(
                backgroundColor: BK_CARD_1,
                textStyle: TextStyle(color: TEXT_COLOR)),
            headerStyle: const CalendarHeaderStyle(
                backgroundColor: BK_APP,
                textStyle: TextStyle(color: TEXT_COLOR)),
            blackoutDatesTextStyle: const TextStyle(color: TEXT_COLOR),
            appointmentTextStyle: const TextStyle(color: TEXT_COLOR),
            viewHeaderStyle: const ViewHeaderStyle(
                backgroundColor: BK_CARD_2,
                dayTextStyle: TextStyle(color: TEXT_COLOR)),

            //monthViewSettings: const MonthViewSettings(
            //appointmentDisplayMode: MonthAppointmentDisplayMode.appointment),
          );
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    setState(() {
      if(mounted){
        tracking.getAllHabitPerMonth(DateTime.now().month, DateTime.now().month).then((value){
          source.appointments = value;
          _calendar = SfCalendar(
            view: CalendarView.month,
            timeSlotViewSettings: const TimeSlotViewSettings(timeIntervalHeight: 10),
            dataSource: source,
            monthViewSettings: const MonthViewSettings(
                showAgenda: true,
                agendaItemHeight: 70,
                agendaStyle: AgendaStyle(
                  backgroundColor: BK_APP,
                  appointmentTextStyle: TextStyle(fontSize: 14, color: TEXT_COLOR),
                  dateTextStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w300, color: TEXT_COLOR),
                  dayTextStyle: TextStyle(fontStyle: FontStyle.normal, fontSize: 20, fontWeight: FontWeight.w700, color: TEXT_COLOR),
                )),
            backgroundColor: BK_APP,
            monthCellBuilder: (BuildContext buildContext, MonthCellDetails details) {
              const Color backgroundColor = BK_CARD_1;
              final Color defaultColor = Theme.of(context).brightness == Brightness.dark ? Colors.black54 : TEXT_COLOR;
              return Container(
                decoration: BoxDecoration(
                    color: backgroundColor,
                    border: Border.all(color: BK_APP, width: 0.5)),
                child: Center(
                  child: Text(details.date.day.toString(), style: const TextStyle(color: TEXT_COLOR),),
                ),);},
            cellBorderColor: BK_CARD_1,
            todayTextStyle: const TextStyle(color: TEXT_COLOR),
            todayHighlightColor: ACCENT_COLOR,
            weekNumberStyle: const WeekNumberStyle(
                backgroundColor: BK_CARD_1,
                textStyle: TextStyle(color: TEXT_COLOR)),
            headerStyle: const CalendarHeaderStyle(
                backgroundColor: BK_APP,
                textStyle: TextStyle(color: TEXT_COLOR)),
            blackoutDatesTextStyle: const TextStyle(color: TEXT_COLOR),
            appointmentTextStyle: const TextStyle(color: TEXT_COLOR),
            viewHeaderStyle: const ViewHeaderStyle(
                backgroundColor: BK_CARD_2,
                dayTextStyle: TextStyle(color: TEXT_COLOR)),

            //monthViewSettings: const MonthViewSettings(
            //appointmentDisplayMode: MonthAppointmentDisplayMode.appointment),
          );
        });
      }
    });
    return Container(
      child: SizedBox(
        height: MediaQuery.of(context).size.height - (BottombarController.height + 25),
        child: ListView(
            children: [
        const ListTile(
        leading: Image(
        image: AssetImage("assets/calendar_logo.png"),
        width: 40,
        height: 40,
      ),
      title: Text(
        "Agenda",
        style: TextStyle(color: TEXT_COLOR, fontWeight: FontWeight.bold),
      ),
    ),
    SizedBox(
    height: MediaQuery.of(context).size.height - (BottombarController.height + 25),
    child: _calendar
    )])));
  }

/*
  Future<List<HabitudeCalendar>> _getDataSource() async {
    //final DateTime today = DateTime.now();
    //List<HabitudeCalendar> list = <HabitudeCalendar>[];
    // tracking.getAllHabitPerMonth(DateTime.now().month, DateTime.now().year).then((value) => source = value.toList());
    return await tracking.getAllHabitPerMonth(DateTime.now().month, DateTime.now().year);
    /*final List<HabitudeCalendar> habitudesList = <HabitudeCalendar>[];
    final DateTime today = DateTime.now();
    final DateTime startTime =
        DateTime(today.year, today.month, today.day, 9, 0, 0);
    final DateTime endTime = startTime.add(const Duration(hours: 2));
*/
    //HabitsDatabase().getHabitsByDateTime(today);
/*
    habitudesList.add(HabitudeCalendar(
        'Mon habitude',
        startTime,
        endTime,
        Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),
        false));
    habitudesList.add(HabitudeCalendar(
        'Mon habitude2',
        startTime,
        endTime,
        Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),
        false));
    habitudesList.add(HabitudeCalendar(
        'Mon habitude3',
        startTime,
        endTime,
        Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),
        false));*/

  }*/
}

class HabitudeCalendarDataSource extends CalendarDataSource {
  HabitudeCalendarDataSource(List<HabitudeCalendar> list) {
    appointments = list;

  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }


}
