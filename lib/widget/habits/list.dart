// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:levelup/database/habits_database.dart';
import 'package:levelup/model/habit.dart';
import 'package:levelup/utils/theme.dart';

import '../../controller/bottombar_controller.dart';
import '../habits_widget.dart';

class HabitList extends StatefulWidget {
  HabitList({Key? key, required this.callFunction}) : super(key: key);
  Function({required HabitSubPages pageName, int? id}) callFunction;
  @override
  _HabitListState createState() => _HabitListState();
}

class _HabitListState extends State<HabitList> {
  late List<Habit>? habits;

  @override
  void initState() {
    habits = null;
    refreshHabits();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future refreshHabits() async {
    var loadHabits = await HabitsDatabase().readAllHabits();
    setState(() {
      habits = loadHabits;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Column(children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Image(
                image: AssetImage("assets/one_habit.png"),
                width: 40,
                height: 40,
              ),
              SizedBox(width: 20),
              Text("Gestion des habitudes",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ))
            ],
          ),
        ),
        // const Divider(
        //   height: 10,
        //   thickness: 5,
        //   indent: 20,
        //   endIndent: 20,
        // ),
        SizedBox(
          height: MediaQuery.of(context).size.height -
              (BottombarController.height + 70),
          child: habits == null
              ? const Text("Chargement")
              : ListView.builder(
                  shrinkWrap: true,
                  itemCount: habits!.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        Card(
                          color: BK_CARD_1,
                          child:
                              Column(mainAxisSize: MainAxisSize.min, children: <
                                  Widget>[
                            ListTile(
                              leading: const Image(
                                image: AssetImage("assets/habit_details.png"),
                                width: 34,
                                height: 34,
                              ),
                              title: Text(
                                habits![index].title,
                                style: const TextStyle(color: TEXT_COLOR),
                              ),
                              subtitle: Text(
                                habits![index].description,
                                style: const TextStyle(color: TEXT_COLOR),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                TextButton(
                                  child: const Text(
                                    'Afficher',
                                    style: TextStyle(color: TEXT_COLOR),
                                  ),
                                  onPressed: () async {
                                    widget.callFunction(
                                        pageName: HabitSubPages.details,
                                        id: habits![index].id);
                                  },
                                ),
                                TextButton(
                                  child: const Text(
                                    'Editer',
                                    style: TextStyle(color: TEXT_COLOR),
                                  ),
                                  onPressed: () async {
                                    widget.callFunction(
                                        pageName: HabitSubPages.edit,
                                        id: habits![index].id);
                                  },
                                ),
                                const SizedBox(width: 8),
                                TextButton(
                                  child: const Text(
                                    'Supprimer',
                                    style: TextStyle(color: TEXT_COLOR),
                                  ),
                                  onPressed: () => showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          AlertDialog(
                                            title: const Text(
                                              "Supprimer l'habitude",
                                              style:
                                                  TextStyle(color: TEXT_COLOR),
                                            ),
                                            backgroundColor: BK_CARD_2,
                                            content: const Text(
                                              "Voulez-vous vraiment supprimer cette habitude ?",
                                              style:
                                                  TextStyle(color: TEXT_COLOR),
                                            ),
                                            actions: <Widget>[
                                              TextButton(
                                                  onPressed: () =>
                                                      Navigator.pop(
                                                          context, "Non"),
                                                  child: const Text("Non",
                                                      style: TextStyle(
                                                          color: TEXT_COLOR))),
                                              TextButton(
                                                  onPressed: () => {
                                                        _deleteHabit(
                                                            habits![index]),
                                                        widget.callFunction(
                                                            pageName:
                                                                HabitSubPages
                                                                    .list),
                                                        Navigator.pop(
                                                            context, "Non")
                                                      },
                                                  child: const Text("Oui",
                                                      style: TextStyle(
                                                          color: TEXT_COLOR))),
                                            ],
                                          )),
                                ),
                                const SizedBox(width: 8),
                              ],
                            ),
                          ]),
                        ),
                        if (index == habits!.length - 1)
                          const SizedBox(height: 80),
                      ],
                    );
                  },
                ),
        ),
        // const Divider(
        //   height: 20,
        //   thickness: 5,
        //   indent: 20,
        //   endIndent: 20,
        // )
      ]),
      Positioned(
        top: MediaQuery.of(context).size.height -
            (BottombarController.height) -
            90,
        right: 10,
        child: FloatingActionButton(
          onPressed: () async {
            widget.callFunction(pageName: HabitSubPages.create);
          },
          backgroundColor: BUTTON_ADD_HABIT,
          child: const Icon(Icons.add),
        ),
      ),
    ]);
  }

  // method that get the current habit
  Future<void> _deleteHabit(Habit habit) async {
    if (habit.id != null) {
      await habit.occurenceDB.delete(habit.id!);
      await HabitsDatabase().delete(habit.id!);
      var loadHabits = await HabitsDatabase().readAllHabits();
      setState(() {
        habits = loadHabits;
      });
    }
  }
}
