import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:levelup/helpers/date_time_helper.dart';
import 'package:levelup/helpers/validate_form_helper.dart';
import 'package:levelup/model/repetition/repetition.dart';
import 'package:levelup/helpers/repetition_helper.dart';
import 'package:levelup/model/repetition/repetition_once_a_week.dart';
import 'package:levelup/utils/theme.dart';
import '../../model/repetition/repetition.dart';
import '../../model/repetition/repetition_once_a_week.dart';

import 'package:levelup/database/habits_database.dart';
import 'package:levelup/model/habit.dart';
import 'package:levelup/widget/habits_widget.dart';

class HabitEditWidget extends StatefulWidget {
  HabitEditWidget({Key? key, required this.callFunction, required this.id})
      : super(key: key);

  final Function({required HabitSubPages pageName, int? id}) callFunction;
  final int? id;

  String? formTitle;
  String? formDescription;
  String? repetitionTypesDropdownValue;
  String? repetitionDaysDropdownValue;
  bool? isAlert;

  @override
  HabitEditWidgetState createState() {
    return HabitEditWidgetState();
  }
}

class HabitEditWidgetState extends State<HabitEditWidget> {
  final _formKey = GlobalKey<FormState>();
  Habit? habit = null;

  String _setStartTime = "", _setStartDate = "";
  String _startHour = "", _startMinute = "", _startTime = "";

  String _setEndTime = "", _setEndDate = "";
  String _endHour = "", _endMinute = "", _endTime = "";

  DateTime selectedStartDate = DateTime.now();
  TimeOfDay selectedStartTime = TimeOfDay(hour: 00, minute: 00);

  DateTime selectedEndDate = DateTime.now();
  TimeOfDay selectedEndTime = TimeOfDay(hour: 00, minute: 00);

  bool isAlertAssigned = false;
  bool isMainDropdownAssigned = false;
  bool isSubDropdownAssigned = false;

  final TextEditingController _startDateController = TextEditingController();
  final TextEditingController _startTimeController = TextEditingController();
  final TextEditingController _endDateController = TextEditingController();
  final TextEditingController _endTimeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    refreshHabit();
  }

  void initDateTime() {
    if (_endDateController.text.isEmpty) {
      _endDateController.text =
          DateTimeHelper.convertDateToTextForDisplay(selectedEndDate);
    }
    if (_endTimeController.text.isEmpty) {
      _endTimeController.text =
          DateTimeHelper.convertTimeToTextForDisplay(selectedEndTime);
    }
    if (_startDateController.text.isEmpty) {
      _startDateController.text =
          DateTimeHelper.convertDateToTextForDisplay(selectedStartDate);
    }
    if (_startTimeController.text.isEmpty) {
      _startTimeController.text =
          DateTimeHelper.convertTimeToTextForDisplay(selectedStartTime);
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    int currentYear = DateTime.now().year;
    const int twoYearsInDays = 730; // 365 * 2
    Duration duration = const Duration(days: twoYearsInDays);

    final DateTime? datePicked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(currentYear),
        lastDate: DateTime.now().add(duration)) as DateTime;

    if (datePicked != null) {
      setState(() {
        selectedEndDate = datePicked;
        _endDateController.text =
            DateTimeHelper.convertDateToTextForDisplay(selectedEndDate);
      });
    }
  }

  Future<void> _selectStartDate(BuildContext context) async {
    int currentYear = DateTime.now().year;
    const int twoYearsInDays = 730; // 365 * 2
    Duration duration = const Duration(days: twoYearsInDays);

    final DateTime? datePicked = await showDatePicker(
        context: context,
        initialDate: selectedStartDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(currentYear),
        lastDate: DateTime.now().add(duration)) as DateTime;

    if (datePicked != null) {
      setState(() {
        selectedStartDate = datePicked;
        _startDateController.text =
            DateTimeHelper.convertDateToTextForDisplay(selectedStartDate);
      });
    }
  }

  Future<void> _selectEndTime(
      BuildContext context, String _hour, String _minute, String _time) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedEndTime,
    );
    if (picked != null) {
      setState(() {
        selectedEndTime = picked;
        _endTimeController.text =
            DateTimeHelper.convertTimeToTextForDisplay(selectedEndTime);
      });
    }
  }

  Future<void> _selectStartTime(
      BuildContext context, String _hour, String _minute, String _time) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedStartTime,
    );
    if (picked != null) {
      setState(() {
        selectedStartTime = picked;
        _startTimeController.text =
            DateTimeHelper.convertTimeToTextForDisplay(selectedStartTime);
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    initDateTime();
    String? repetitionTypeFromDB =
        habit?.repetition.getRepetitionTypeFromString();
    String? repetitionDayFromDB = habit?.repetition.getRepetitionValueDB();

    String? startDate = habit?.startDate.toString();
    String? endDate = habit?.endDate.toString();

    if (_startDateController.text.isEmpty) {
      _startDateController.text = startDate!.split(" ")[0];
    }
    if (_startTimeController.text.isEmpty) {
      _startTimeController.text = startDate!.split(" ")[1];
    }

    if (_endDateController.text.isEmpty) {
      _endDateController.text = endDate!.split(" ")[0];
    }
    if (_endTimeController.text.isEmpty) {
      _endTimeController.text = endDate!.split(" ")[1];
    }

    // set the value of the alert once
    if (habit?.isAlert != null && isAlertAssigned == false) {
      widget.isAlert = habit?.isAlert;
      isAlertAssigned = true;
    }

    // Set main dropdown with db values
    if (repetitionTypeFromDB != null && isMainDropdownAssigned == false) {
      widget.repetitionTypesDropdownValue =
          RepetitionHelper.ConvertRepetitionTypesEnToFr(repetitionTypeFromDB);
      isMainDropdownAssigned = true;
    } else if (widget.repetitionTypesDropdownValue == null || widget.repetitionTypesDropdownValue == "") {
      widget.repetitionTypesDropdownValue = RepetitionTypes.NO_REPEAT_FR;
    }

    // Set sub dropdown with db values
    if (repetitionDayFromDB != null && isSubDropdownAssigned == false) {
      widget.repetitionDaysDropdownValue =
          RepetitionHelper.ConvertRepetitionDaysEnToFr(repetitionDayFromDB);
      isSubDropdownAssigned = true;
    } else if (widget.repetitionDaysDropdownValue == null || widget.repetitionDaysDropdownValue == "") {
      widget.repetitionDaysDropdownValue = RepetitionOnceAWeek.MONDAY_FR;
    }

    return Form(
      key: _formKey,
      child: habit == null
          ? const CircularProgressIndicator()
          : SafeArea(
              child: Column(
                children: <Widget>[
                  // ----------
                  // Main title
                  // ----------
                  const SizedBox(
                    width: double.infinity,
                    height: 40,
                    child: Center(
                      child: Text("Éditez l'habitude",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                    ),
                  ),
                  const SizedBox(height: 15),
                  // ----------
                  // Habit title
                  // ----------
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 42),
                    height: 20,
                    child: const Text(
                      "Titre",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  // ----------
                  // Habit title field
                  // ----------
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 5, right: 40),
                    height: 50,
                    child: TextFormField(
                      decoration: const InputDecoration(
                        icon: Icon(Icons.title, color: Colors.white,),
                        hintText: "Entrez votre titre",
                          hintStyle: TextStyle(color: Colors.grey)
                      ),
                      validator: (String? value) {
                        CheckValidity titleValidity =
                            ValidateForm.checkTitle(value!);
                        if (titleValidity.isNotValid) {
                          return titleValidity.message;
                        }
                      },
                      initialValue: habit!.title,
                      onChanged: (value) {
                        widget.formTitle = value;
                      },
                    ),
                  ),
                  const SizedBox(height: 10),
                  // ----------
                  // Habit description title
                  // ----------
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 42),
                    height: 20,
                    child: const Text(
                      "Description",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  // ----------
                  // Habit description field
                  // ----------
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 5, right: 40),
                    height: 50,
                    child: TextFormField(
                      decoration: const InputDecoration(
                        icon: Icon(Icons.description, color: Colors.white,),
                        hintText: "Entrez votre description",
                          hintStyle: TextStyle(color: Colors.grey)
                      ),
                      validator: (String? value) {
                        CheckValidity descriptionValidity =
                            ValidateForm.checkDescription(value!);
                        if (descriptionValidity.isNotValid) {
                          return descriptionValidity.message;
                        }
                      },
                      initialValue: habit!.description,
                      onChanged: (value) {
                        widget.formDescription = value;
                      },
                    ),
                  ),
                  const SizedBox(height: 10),
                  // ----------
                  // Habit main dropdown title
                  // ----------
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 42),
                    height: 20,
                    child: const Text(
                      "Mode de répétition",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  // ----------
                  // Habit main dropdown field
                  // ----------
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 40, right: 140),
                    height: 50,
                    child: DropdownButton<String>(
                        value: widget.repetitionTypesDropdownValue,
                        icon: const Icon(Icons.repeat),
                        iconSize: 24,
                        elevation: 16,
                        style: const TextStyle(color: DESCRIPTION_CARD),
                        underline: Container(
                          height: 2,
                          color: DESCRIPTION_CARD,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            widget.repetitionTypesDropdownValue =
                                newValue ?? "";
                          });
                        },
                        items: RepetitionTypes.listFR
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList()),
                  ),
                  // ----------
                  // Habit sub dropdown field
                  // ----------
                  widget.repetitionTypesDropdownValue ==
                          RepetitionTypes.ONCE_A_WEEK_FR
                      ? Container(
                          width: double.infinity,
                          margin: const EdgeInsets.only(left: 40, right: 245),
                          height: 50,
                          child: DropdownButton<String>(
                              value: widget.repetitionDaysDropdownValue,
                              icon: const Icon(Icons.calendar_today),
                              iconSize: 24,
                              elevation: 16,
                              style: const TextStyle(color: DESCRIPTION_CARD),
                              underline: Container(
                                height: 2,
                                color: DESCRIPTION_CARD,
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  widget.repetitionDaysDropdownValue =
                                      newValue ?? "";
                                });
                              },
                              items: RepetitionOnceAWeek.listFR
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList()),
                        )
                      : const SizedBox.shrink(),
                  const SizedBox(height: 10),
                  // ----------
                  // Habit start, end date and time
                  // ----------
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        children: [
                          Container(
                            width: 180,
                            height: 70,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                const Text(
                                  "Date de départ",
                                  style: TextStyle(fontSize: 16),
                                ),
                                InkWell(
                                  onTap: () {
                                    _selectStartDate(context);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: TextFormField(
                                      style: const TextStyle(fontSize: 15, color: Colors.white),
                                      textAlign: TextAlign.center,
                                      enabled: false,
                                      keyboardType: TextInputType.text,
                                      controller: _startDateController,
                                      onSaved: (String? val) {
                                        _setStartDate = val!;
                                      },
                                      decoration: const InputDecoration(
                                          disabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide.none),
                                          // labelText: 'Time',
                                          contentPadding:
                                          EdgeInsets.only(top: 0.0)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 180,
                            height: 70,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                const Text(
                                  "Heure de départ",
                                  style: TextStyle(fontSize: 16),
                                ),
                                InkWell(
                                  onTap: () {
                                    _selectStartTime(
                                        context, _startHour, _startMinute, _startTime);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: TextFormField(
                                      style: const TextStyle(fontSize: 15, color: Colors.white),
                                      textAlign: TextAlign.center,
                                      onSaved: (String? val) {
                                        _setStartTime = val!;
                                      },
                                      enabled: false,
                                      keyboardType: TextInputType.text,
                                      controller: _startTimeController,
                                      decoration: const InputDecoration(
                                          disabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide.none),
                                          contentPadding: EdgeInsets.all(5)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            width: 180,
                            height: 70,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                const Text(
                                  "Date de fin",
                                  style: TextStyle(fontSize: 16),
                                ),
                                InkWell(
                                  onTap: () {
                                    _selectEndDate(context);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: TextFormField(
                                      style: const TextStyle(fontSize: 15, color: Colors.white),
                                      textAlign: TextAlign.center,
                                      enabled: false,
                                      keyboardType: TextInputType.text,
                                      controller: _endDateController,
                                      onSaved: (String? val) {
                                        _setEndDate = val!;
                                      },
                                      decoration: const InputDecoration(
                                          disabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide.none),
                                          // labelText: 'Time',
                                          contentPadding:
                                          EdgeInsets.only(top: 0.0)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 180,
                            height: 70,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                const Text(
                                  "Heure de fin",
                                  style: TextStyle(fontSize: 16),
                                ),
                                InkWell(
                                  onTap: () {
                                    _selectEndTime(
                                        context, _endHour, _endMinute, _endTime);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: TextFormField(
                                      style: const TextStyle(fontSize: 15, color: Colors.white),
                                      textAlign: TextAlign.center,
                                      onSaved: (String? val) {
                                        _setEndTime = val!;
                                      },
                                      enabled: false,
                                      keyboardType: TextInputType.text,
                                      controller: _endTimeController,
                                      decoration: const InputDecoration(
                                          disabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide.none),
                                          contentPadding: EdgeInsets.all(5)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 40,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text(
                          "Alerte on/off : ",
                          style: TextStyle(fontSize: 16),
                        ),
                        Switch(
                          value: widget.isAlert != null ? widget.isAlert! : false,
                          onChanged: (value) {
                            setState(() {
                              widget.isAlert = value;
                            });
                          },
                          activeTrackColor: Colors.blue,
                          activeColor: Colors.blueAccent,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ElevatedButton(
                        onPressed: () async {
                          widget.callFunction(pageName: HabitSubPages.list);
                        },
                        child: const Text("Retour à la liste"),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(BK_CARD_2),
                        ),
                      ),
                      const SizedBox(width: 20),
                      ElevatedButton(
                        onPressed: () => {
                          update(habit!),
                        },
                        child: const Text("Sauvegarder"),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(BK_CARD_2),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
    );
  }

  Future<void> refreshHabit() async {
    var loadHabit = await HabitsDatabase().readHabit(widget.id!);
    setState(() {
      habit = loadHabit;

      selectedStartDate = loadHabit.startDate;
      selectedStartTime = TimeOfDay(
          hour: loadHabit.startDate.hour, minute: loadHabit.startDate.minute);

      selectedEndDate = loadHabit.endDate;
      selectedEndTime = TimeOfDay(
          hour: loadHabit.endDate.hour, minute: loadHabit.endDate.minute);

      _startDateController.text =
          DateTimeHelper.convertDateToTextForDisplay(selectedStartDate);
      _startTimeController.text =
          DateTimeHelper.convertTimeToTextForDisplay(selectedStartTime);

      _endDateController.text =
          DateTimeHelper.convertDateToTextForDisplay(selectedEndDate);
      _endTimeController.text =
          DateTimeHelper.convertTimeToTextForDisplay(selectedEndTime);
    });
  }

  Future update(Habit habit) async {
    CheckValidity dateValidity =
        ValidateForm.checkDates(habit.startDate, selectedEndDate);

    widget.formTitle ??= habit.title;
    widget.formDescription ??= habit.description;

    habit = Habit.fromDB(
        id: habit.id,
        title: widget.formTitle,
        description: widget.formDescription,
        repetition: RepetitionHelper.ConvertRepetitionValueForDatabase(
            widget.repetitionTypesDropdownValue!,
            widget.repetitionDaysDropdownValue!),
        dateStart: DateTimeHelper.convertToDateTime(selectedStartDate, selectedStartTime),
        dateEnd:
            DateTimeHelper.convertToDateTime(selectedEndDate, selectedEndTime),
        alert: widget.isAlert);

    if (_formKey.currentState!.validate() && dateValidity.isNotValid == false) {
      HabitsDatabase().update(habit);
      widget.callFunction(pageName: HabitSubPages.list);
    } else if (dateValidity.isNotValid == true) {
      final snackBar = SnackBar(
        content: Text(dateValidity.message),
        duration: const Duration(seconds: 5),
        action: SnackBarAction(
          label: 'OK',
          onPressed: () {},
        ),
      );

      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
