import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:levelup/helpers/date_time_helper.dart';
import 'package:levelup/helpers/validate_form_helper.dart';
import 'package:levelup/utils/theme.dart';
import '../../model/repetition/repetition.dart';
import 'package:levelup/helpers/repetition_helper.dart';
import '../../model/repetition/repetition_once_a_week.dart';
import 'package:levelup/database/habits_database.dart';
import 'package:levelup/model/habit.dart';
import 'package:levelup/widget/habits_widget.dart';

class HabitCreateForm extends StatefulWidget {
  HabitCreateForm({Key? key, required this.callFunction}) : super(key: key);

  final Function({required HabitSubPages pageName, int? id}) callFunction;

  String formTitle = "";
  String formDescription = "";
  String repetitionTypesDropdownValue = RepetitionTypes.NO_REPEAT_FR;
  String repetitionDaysDropdownValue = RepetitionOnceAWeek.MONDAY_FR;
  String? repetitionResult;
  bool isAlert = false;

  @override
  HabitCreateFormState createState() {
    return HabitCreateFormState();
  }
}

class HabitCreateFormState extends State<HabitCreateForm> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _startDateController = TextEditingController();
  final TextEditingController _startTimeController = TextEditingController();
  final TextEditingController _endDateController = TextEditingController();
  final TextEditingController _endTimeController = TextEditingController();

  // Other fields
  final TextEditingController formTitleController = TextEditingController();

  DateTime selectedStartDate = DateTime.now();
  TimeOfDay selectedStartTime = const TimeOfDay(hour: 00, minute: 00);
  DateTime selectedEndDate = DateTime.now();
  TimeOfDay selectedEndTime = const TimeOfDay(hour: 00, minute: 00);

  String _startHour = "", _startMinute = "", _startTime = "";
  String _endHour = "", _endMinute = "", _endTime = "";

  void initDateTime() {
    // init date
    if (_startDateController.text.isEmpty) {
      _startDateController.text =
          DateTimeHelper.convertDateToTextForDisplay(selectedStartDate);
    }
    if (_endDateController.text.isEmpty) {
      _endDateController.text =
          DateTimeHelper.convertDateToTextForDisplay(selectedEndDate);
    }
    // init time
    if (_startTimeController.text.isEmpty) {
      _startTimeController.text =
          DateTimeHelper.convertTimeToTextForDisplay(selectedStartTime);
    }
    if (_endTimeController.text.isEmpty) {
      _endTimeController.text =
          DateTimeHelper.convertTimeToTextForDisplay(selectedEndTime);
    }
  }

  Future<void> _selectStartDate(BuildContext context) async {
    int currentYear = DateTime.now().year;
    const int twoYearsInDays = 730; // 365 * 2
    Duration duration = const Duration(days: twoYearsInDays);

    final DateTime? datePicked = await showDatePicker(
        context: context,
        initialDate: selectedStartDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(currentYear),
        lastDate: DateTime.now().add(duration));

    if (datePicked != null) {
      setState(() {
        selectedStartDate = datePicked;
        _startDateController.text =
            DateTimeHelper.convertDateToTextForDisplay(selectedStartDate);
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    int currentYear = DateTime.now().year;
    const int twoYearsInDays = 730; // 365 * 2
    Duration duration = const Duration(days: twoYearsInDays);
    print("picker date");
    print(duration);
    final DateTime? datePicked = await showDatePicker(
        context: context,
        initialDate: selectedEndDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(currentYear),
        lastDate: DateTime.now().add(duration)) as DateTime;

    if (datePicked != null) {
      setState(() {
        selectedEndDate = datePicked;
        _endDateController.text =
            DateTimeHelper.convertDateToTextForDisplay(selectedEndDate);
      });
    }
  }

  Future<void> _selectStartTime(
      BuildContext context, String _hour, String _minute, String _time) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedStartTime,
    );
    if (picked != null) {
      setState(() {
        selectedStartTime = picked;
        _startTimeController.text =
            DateTimeHelper.convertTimeToTextForDisplay(selectedStartTime);
      });
    }
  }

  Future<void> _selectEndTime(
      BuildContext context, String _hour, String _minute, String _time) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedEndTime,
    );
    if (picked != null) {
      setState(() {
        selectedEndTime = picked;
        _endTimeController.text =
            DateTimeHelper.convertTimeToTextForDisplay(selectedEndTime);
      });
    }
  }

  @override
  void initState() {
    super.initState();
  }

  void formValidation() {
    final form = _formKey.currentState;
    if (form!.validate()) {
      print("Validation OK");
    } else {
      print("Validation NOT OK");
    }
  }

  @override
  Widget build(BuildContext context) {
    initDateTime();
    Habit createdHabit;

    return Form(
      key: _formKey,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            // ----------
            // Main title
            // ----------
            Container(
              width: double.infinity,
              height: 40,
              child: const Center(
                child: Text("Créez votre habitude",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              ),
            ),
            const SizedBox(height: 15),
            // ----------
            // Habit title
            // ----------
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 42),
              height: 20,
              child: const Text(
                "Titre",
                style: TextStyle(fontSize: 18),
              ),
            ),
            // ----------
            // Habit title field
            // ----------
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 5, right: 40),
              height: 50,
              child: TextFormField(
                decoration: const InputDecoration(
                  fillColor: Colors.black,
                  icon: Icon(Icons.title, color: ICON_COLOR,),
                  hintText: "Entrez votre titre",
                  hintStyle: TextStyle(color: SUB_TITLE)
                ),
                controller: formTitleController,
                validator: (String? value) {
                  CheckValidity titleValidity = ValidateForm.checkTitle(value!);
                  if (titleValidity.isNotValid) {
                    return titleValidity.message;
                  }
                },
                onChanged: (value) {
                  widget.formTitle = value;
                },
              ),
            ),
            const SizedBox(height: 10),
            // ----------
            // Habit description title
            // ----------
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 42),
              height: 20,
              child: const Text(
                "Description",
                style: TextStyle(fontSize: 18, color: TEXT_COLOR),
              ),
            ),
            // ----------
            // Habit description field
            // ----------
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 5, right: 40),
              height: 50,
              child: TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.description, color: ICON_COLOR,),
                  hintText: "Entrez votre description",
                    hintStyle: TextStyle(color: SUB_TITLE)
                ),
                validator: (String? value) {
                  CheckValidity descriptionValidity =
                      ValidateForm.checkDescription(value!);
                  if (descriptionValidity.isNotValid) {
                    return descriptionValidity.message;
                  }
                },
                onChanged: (value) {
                  widget.formDescription = value;
                },
              ),
            ),
            const SizedBox(height: 10),
            // ----------
            // Habit main dropdown title
            // ----------
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 42),
              height: 20,
              child: const Text(
                "Mode de répétition",
                style: TextStyle(fontSize: 18, color: TEXT_COLOR),
              ),
            ),
            // ----------
            // Habit main dropdown field
            // ----------
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 40, right: 140),
              height: 50,
              child: DropdownButton<String>(
                  value: widget.repetitionTypesDropdownValue,
                  icon: const Icon(Icons.repeat),
                  iconSize: 24,
                  elevation: 16,
                  style: const TextStyle(color: DESCRIPTION_CARD),
                  underline: Container(
                    height: 2,
                    color: DESCRIPTION_CARD,
                  ),
                  onChanged: (String? newValue) {
                    setState(() {
                      widget.repetitionTypesDropdownValue = newValue ?? "";
                    });
                  },
                  items: RepetitionTypes.listFR
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList()),
            ),
            const SizedBox(height: 10),
            // ----------
            // Habit sub-dropdown title
            // ----------
            widget.repetitionTypesDropdownValue ==
                    RepetitionTypes.ONCE_A_WEEK_FR
                ? Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 42),
                    height: 20,
                    child: const Text(
                      "Jour de répétition",
                      style: TextStyle(fontSize: 18),
                    ),
                  )
                : const SizedBox.shrink(),
            // ----------
            // Habit sub dropdown field
            // ----------
            widget.repetitionTypesDropdownValue ==
                    RepetitionTypes.ONCE_A_WEEK_FR
                ? Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 40, right: 245),
                    height: 50,
                    child: DropdownButton<String>(
                        value: widget.repetitionDaysDropdownValue,
                        icon: const Icon(Icons.calendar_today),
                        iconSize: 24,
                        elevation: 16,
                        style: const TextStyle(color: DESCRIPTION_CARD),
                        underline: Container(
                          height: 2,
                          color: DESCRIPTION_CARD,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            widget.repetitionDaysDropdownValue = newValue ?? "";
                          });
                        },
                        items: RepetitionOnceAWeek.listFR
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList()),
                  )
                : const SizedBox.shrink(),
            const SizedBox(height: 10),
            // ----------
            // Habit start and end date title
            // ----------
            const SizedBox(height: 10),
            // ----------
            // Habit start and end date picker
            // ----------
            SizedBox(
              width: double.infinity,
              height: 110,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      const Text("Date de départ"),
                      InkWell(
                        onTap: () {
                          _selectStartDate(context);
                        },
                        child: Container(
                          width: 100,
                          height: 35,
                          alignment: Alignment.center,
                          child: TextFormField(
                            style: const TextStyle(fontSize: 15, color: TEXT_COLOR),
                            textAlign: TextAlign.center,
                            enabled: false,
                            keyboardType: TextInputType.text,
                            controller: _startDateController,
                            decoration: const InputDecoration(
                                disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide.none),
                                // labelText: 'Time',
                                contentPadding: EdgeInsets.only(top: 0.0)),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text("Heure de départ"),
                      InkWell(
                        onTap: () {
                          _selectStartTime(
                              context, _startHour, _startMinute, _startTime);
                        },
                        child: Container(
                          width: 100,
                          height: 29,
                          alignment: Alignment.center,
                          child: TextFormField(
                            style: const TextStyle(fontSize: 15, color: Colors.white),
                            textAlign: TextAlign.center,
                            validator: (String? value) => "Show my Error",
                            onSaved: (String? val) {
                              _startTime = val!;
                            },
                            enabled: false,
                            keyboardType: TextInputType.text,
                            controller: _startTimeController,
                            decoration: const InputDecoration(
                                disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide.none),
                                contentPadding: EdgeInsets.all(5)),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      const Text("Date de fin"),
                      InkWell(
                        onTap: () {
                          _selectEndDate(context);
                        },
                        child: Container(
                          width: 100,
                          height: 35,
                          alignment: Alignment.center,
                          child: TextFormField(
                            style: const TextStyle(fontSize: 15, color: Colors.white),
                            textAlign: TextAlign.center,
                            enabled: false,
                            keyboardType: TextInputType.text,
                            controller: _endDateController,
                            decoration: const InputDecoration(
                                disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide.none),
                                // labelText: 'Time',
                                contentPadding: EdgeInsets.only(top: 0.0)),
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text("Heure de fin"),
                      InkWell(
                        onTap: () {
                          _selectEndTime(
                              context, _endHour, _endMinute, _endTime);
                        },
                        child: Container(
                          width: 100,
                          height: 29,
                          alignment: Alignment.center,
                          child: TextFormField(
                            style: const TextStyle(fontSize: 15, color: Colors.white),
                            textAlign: TextAlign.center,
                            enabled: false,
                            keyboardType: TextInputType.text,
                            controller: _endTimeController,
                            decoration: const InputDecoration(
                                disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide.none),
                                contentPadding: EdgeInsets.all(5)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 12),
            // ----------
            // Habit alert and submit button
            // ----------
            SizedBox(
              width: double.infinity,
              height: 70,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(height: 1),
                        const Text("Alerte on/off"),
                        Switch(
                          value: widget.isAlert,
                          onChanged: (value) {
                            setState(() {
                              widget.isAlert = value;
                            });
                          },
                          activeTrackColor: Colors.blue,
                          activeColor: Colors.blueAccent,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: double.infinity,
                    child: Row(
                      children: <Widget>[
                        ElevatedButton(
                            onPressed: () => {
                                  widget.repetitionResult = RepetitionHelper
                                      .ConvertRepetitionValueForDatabase(
                                          widget.repetitionTypesDropdownValue,
                                          widget.repetitionDaysDropdownValue),

                                  // final habit creation
                                  createdHabit = Habit.fromDB(
                                    title: widget.formTitle,
                                    description: widget.formDescription,
                                    repetition: widget.repetitionResult,
                                    dateStart: DateTimeHelper.convertToDateTime(
                                        selectedStartDate, selectedStartTime),
                                    dateEnd: DateTimeHelper.convertToDateTime(
                                        selectedEndDate, selectedEndTime),
                                    alert: widget.isAlert,
                                  ),

                                  isHabitValid(),
                                  addHabit(createdHabit),
                                },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(BK_CARD_2),
                            ),
                            child: const Text("Ajouter")),
                        const SizedBox(width: 20),
                        ElevatedButton(
                          onPressed: () async {
                            widget.callFunction(pageName: HabitSubPages.list);
                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(BK_CARD_2),
                          ),
                          child: const Text("Retour"),
                        ),
                      ],
                    ),
                    width: 180,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isHabitValid() {
    return _formKey.currentState!.validate();
  }

  Future addHabit(Habit habit) async {
    CheckValidity dateValidity =
        ValidateForm.checkDates(habit.startDate, habit.endDate);

    if (dateValidity.isNotValid == false) {
      HabitsDatabase().create(habit);
      widget.callFunction(pageName: HabitSubPages.list);
    } else if (dateValidity.isNotValid) {
      final snackBar = SnackBar(
        content: Text(dateValidity.message),
        duration: const Duration(seconds: 5),
        action: SnackBarAction(
          label: 'OK',
          onPressed: () {},
        ),
      );

      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
