import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:levelup/model/habit.dart';
import 'package:levelup/utils/theme.dart';
import '../../model/repetition/repetition.dart';
import '../../model/repetition/repetition_once_a_week.dart';
import 'package:levelup/database/habits_database.dart';

import '../habits_widget.dart';

class HabitDetails extends StatefulWidget {
  const HabitDetails({Key? key, required this.callFunction, required this.id})
      : super(key: key);

  final Function({required HabitSubPages pageName, int? id}) callFunction;
  final int? id;

  @override
  _HabitDetailsState createState() => _HabitDetailsState();
}

class _HabitDetailsState extends State<HabitDetails> {
  Habit? habit = null;

  @override
  void initState() {
    super.initState();

    refreshHabit();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          // ----------
          // Main title
          // ----------
          const SizedBox(
            width: double.infinity,
            height: 40,
            child: Center(
              child: Text(
                  "Détails de l'habitude",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            ),
          ),
          const SizedBox(height: 20),

          habit == null ?
          const CircularProgressIndicator() :
          Card(
            color: BK_CARD_1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                  leading: const Image(
                    image: AssetImage("assets/habit_details.png"),
                    width: 40,
                    height: 40,
                  ),
                  title: Text(
                      habit!.title,
                      style: const TextStyle(fontSize: 20, color: TEXT_COLOR),
                  ),
                  subtitle: Text(
                      habit!.description,
                      style: const TextStyle(fontSize: 16, fontStyle: FontStyle.italic, color: TEXT_COLOR),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: const <Widget>[
                              SizedBox(
                                height: 15,
                              )
                            ],
                          ),
                          Text(
                              "Type de répétition : " + displayRepetitionType(),
                              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w300, color: TEXT_COLOR),
                          ),
                          const SizedBox(height: 15),
                          habit!.repetition.getRepetitionValueDB() != "" ?
                          Text(
                              "Jour de répétition: " + displayRepetitionDay(),
                              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w300, color: TEXT_COLOR),
                          ) : const SizedBox(),
                          const SizedBox(height: 15),
                          Text(
                            "Alerte : " + displayAlert(),
                            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w300, color: TEXT_COLOR),
                          ),
                          const SizedBox(height: 15),
                          Text(
                            "Date de départ : " + displayDate(habit!.startDate),
                            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w300, color: TEXT_COLOR),
                          ),
                          const SizedBox(height: 15),
                          Text(
                            "Date de fin : " + displayDate(habit!.endDate),
                            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w300, color: TEXT_COLOR),
                          ),
                          const SizedBox(height: 15),
                          TextButton(
                            child: const Text('Revenir à la liste', style: TextStyle(color: CHECK_WAITING),),
                            onPressed: () async {
                              widget.callFunction(pageName: HabitSubPages.list);
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// Convert the repetition type to a french value
  String displayRepetitionType() {
    String repetitionType = habit!.repetition.getRepetitionTypeFromString();

    if (repetitionType == RepetitionTypes.NO_REPEAT_EN) {
      return RepetitionTypes.NO_REPEAT_FR;
    } else if (repetitionType == RepetitionTypes.ONCE_A_WEEK_EN) {
      return RepetitionTypes.ONCE_A_WEEK_FR;
    } else if (repetitionType == RepetitionTypes.EVERYDAY_EN) {
      return RepetitionTypes.EVERYDAY_FR;
    } else {
      throw Error();
    }
  }

  /// Convert the repetition day to a french value
  String displayRepetitionDay() {
    String repetitionDay = habit!.repetition.getRepetitionValueDB();

    switch(repetitionDay){
      case RepetitionOnceAWeek.MONDAY_EN : {return RepetitionOnceAWeek.MONDAY_FR;} break;
      case RepetitionOnceAWeek.TUESDAY_EN : {return RepetitionOnceAWeek.TUESDAY_FR;} break;
      case RepetitionOnceAWeek.WEDNESDAY_EN : {return RepetitionOnceAWeek.WEDNESDAY_FR;} break;
      case RepetitionOnceAWeek.THURSDAY_EN : {return RepetitionOnceAWeek.THURSDAY_FR;} break;
      case RepetitionOnceAWeek.FRIDAY_EN : {return RepetitionOnceAWeek.FRIDAY_FR;} break;
      case RepetitionOnceAWeek.SATURDAY_EN : {return RepetitionOnceAWeek.SATURDAY_FR;} break;
      case RepetitionOnceAWeek.SUNDAY_EN : {return RepetitionOnceAWeek.SUNDAY_FR;} break;
      default : {return "";} break;
    }
  }


  /// Convert a boolean to french alert value
  String displayAlert() {
    bool alert = habit!.isAlert;

    if (alert == true) {
      return "Activée";
    } else {
      return "Désactivée";
    }
  }

  /// Display the date in a string format
  String displayDate(DateTime date) {
    String year, month, day;
    String hour, minute, seconds;

    year = date.year.toString();
    month = date.month.toString();
    day = date.day.toString();

    hour = date.hour.toString();
    minute = date.minute.toString();
    seconds = date.second.toString();

    return year + "-" + month + "-" + day + " " + hour + ":" + minute;
  }

  /// Method that get the current habit
  Future<void> refreshHabit() async {
    var loadHabit = await HabitsDatabase().readHabit(widget.id!);
    setState(() {
      habit = loadHabit;
    });
  }
}

