import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:levelup/components/homepage.dart';
import 'package:levelup/model/route_model.dart';
import 'package:levelup/model/routes.dart';
import 'package:levelup/controller/bottombar_controller.dart';

void main() {
  int selectedIndex = 0;
  void updateRoute(int index) {
    expect(index, selectedIndex);
  }

  // Create app
  const baseApp = MediaQuery(
      data: MediaQueryData(),
      child: MaterialApp(
        home: Homepage(),
      ));

  // Create just bottomBar
  final bottomBar = MediaQuery(
      data: const MediaQueryData(),
      child: MaterialApp(
        home: BottombarController(
          refresh: updateRoute,
        ),
      ));
  // Get routes
  List<RouteModel> routes = Routes.routes;
  testWidgets("BottombarController test", (WidgetTester tester) async {
    await tester.pumpWidget(baseApp);
    // on vérifie si la bottomBar existe bien une fois
    final bottomBar = find.byType(BottombarController);
    expect(bottomBar, findsOneWidget);

    // final bottomState = tester.state(bottomBar) as BottombarControllerState;
    // for (var i = 0; i < routes.length; i++) {
    //   await tester.tap(find.byIcon(routes[i].icon.icon as IconData));
    //   await tester.pump();
    //   //expect(bottomState.selectedIndex, i);
    // }
  });
  testWidgets("vérifie si les boutons correspondent aux routes",
      (WidgetTester tester) async {
    await tester.pumpWidget(bottomBar);
    for (var i = 0; i < routes.length; i++) {
      expect(find.text(routes[i].name), findsOneWidget);
    }
  });

  testWidgets("vérifie si les icones correspondent aux routes",
      (WidgetTester tester) async {
    await tester.pumpWidget(bottomBar);
    for (var i = 0; i < routes.length; i++) {
      expect(find.byIcon(routes[i].icon.icon as IconData), findsOneWidget);
    }
  });
}
